AWS Certified Solutions Architect Associate
===========================================

Terminology
-----------

-   **ACL** Access Control List - *A document that defines who can
    access a particular bucket or object. Each bucket and object in
    Amazon S3 has an ACL. The document defines what each type of user
    can do, such as write and read permissions.*
-   **Amazon API Gateway** - *A fully managed service that makes it easy
    for developers to create, publish, maintain, monitor, and secure
    APIs at any scale.*
-   **Amazon AppStream** - *A web service for streaming existing Windows
    applications from the cloud to any device.*
-   **Amazon Aurora** - *A fully managed MySQL-compatible relational
    database engine that combines the speed and availability of
    commercial databases with the simplicity and cost-effectiveness of
    open source databases.*
-   **Amazon CloudFront** - *An AWS content delivery service that helps
    you improve the performance, reliability, and availability of your
    websites and applications.*
-   **Amazon CloudWatch** - *A web service that enables you to monitor
    and manage various metrics, and configure alarm actions based on
    data from those metrics.*
-   **Amazon Cognito** - *A web service that makes it easy to save
    mobile user data, such as app preferences or game state, in the AWS
    cloud without writing any back-end code or managing any
    infrastructure. Amazon Cognito offers mobile identity management and
    data synchronization across devices.*
-   **Amazon DynamoDB** - *A fully managed NoSQL database service that
    provides fast and predictable performance with seamless
    scalability.*
-   **Amazon Elastic Block Store (Amazon EBS)** - *A service that
    provides block level storage volumes for use with EC2 instances.*
-   **Amazon EBS-backed AMI** - *A type of Amazon Machine Image (AMI)
    whose instances use an Amazon EBS volume as their root device.
    Compare this with instances launched from instance store-backed
    AMIs, which use the instance store as the root device.*
-   **Amazon Elastic Compute Cloud (Amazon EC2)** - *A web service that
    enables you to launch and manage Linux/UNIX and Windows server
    instances in Amazon's data centers.*
-   **Amazon EC2 Auto Scaling** - *A web service designed to launch or
    terminate instances automatically based on user-defined policies,
    schedules, and health checks.*
-   **Amazon Elastic File System (Amazon EFS)** - *A file storage
    service for EC2 instances. Amazon EFS is easy to use and provides a
    simple interface with which you can create and configure file
    systems. Amazon EFS storage capacity grows and shrinks automatically
    as you add and remove files.*
-   **Amazon EMR (Amazon EMR)** - *A web service that makes it easy to
    process large amounts of data efficiently. Amazon EMR uses Hadoop
    processing combined with several AWS products to do such tasks as
    web indexing, data mining, log file analysis, machine learning,
    scientific simulation, and data warehousing.*
-   **Amazon Elastic Transcoder** - *A cloud-based media transcoding
    service. Elastic Transcoder is a highly scalable tool for converting
    (or transcoding) media files from their source format into versions
    that will play on devices like smartphones, tablets, and PCs.*
-   **Amazon ElastiCache** - *A web service that simplifies deploying,
    operating, and scaling an in-memory cache in the cloud. The service
    improves the performance of web applications by providing
    information retrieval from fast, managed, in-memory caches, instead
    of relying entirely on slower disk-based databases.*
-   **Glacier** - *A secure, durable, and low-cost storage service for
    data archiving and long-term backup. You can reliably store large or
    small amounts of data for significantly less than on-premises
    solutions. Glacier is optimized for infrequently accessed data,
    where a retrieval time of several hours is suitable.*
-   **Amazon Kinesis** - *A platform for streaming data on AWS. Kinesis
    offers services that simplify the loading and analysis of streaming
    data.*
-   **Amazon Kinesis Data Firehose** - *A fully managed service for
    loading streaming data into AWS. Kinesis Data Firehose can capture
    and automatically load streaming data into Amazon S3 and Amazon
    Redshift , enabling near real-time analytics with existing business
    intelligence tools and dashboards. Kinesis Data Firehose
    automatically scales to match the throughput of your data and
    requires no ongoing administration. It can also batch, compress, and
    encrypt the data before loading it.*
-   **Amazon Kinesis Data Streams** - *A web service for building custom
    applications that process or analyze streaming data for specialized
    needs. Amazon Kinesis Data Streams can continuously capture and
    store terabytes of data per hour from hundreds of thousands of
    sources.*
-   **Amazon Machine Image (AMI)** - *An encrypted machine image stored
    in Amazon Elastic Block Store (Amazon EBS) or Amazon Simple Storage
    Service. AMIs are like a template of a computer's root drive. They
    contain the operating system and can also include software and
    layers of your application, such as database servers, middleware,
    web servers, and so on.*
-   **Amazon Redshift** - *A fully managed, petabyte-scale data
    warehouse service in the cloud. With Amazon Redshift, you can
    analyze your data using your existing business intelligence tools.*
-   **Amazon Relational Database Service (Amazon RDS)** - *A web service
    that makes it easier to set up, operate, and scale a relational
    database in the cloud. It provides cost-efficient, resizable
    capacity for an industry-standard relational database and manages
    common database administration tasks.*
-   **Amazon Resource Name (ARN)** - *A standardized way to refer to an
    AWS resource. For example:
    arn:aws:iam::123456789012:user/division\_abc/subdivision\_xyz/Bob.*
-   **Amazon Route 53** - *A web service you can use to create a new DNS
    service or to migrate your existing DNS service to the cloud.*
-   **Amazon Simple Storage Service (Amazon S3)** - *Storage for the
    internet. You can use it to store and retrieve any amount of data at
    any time, from anywhere on the web.*
-   **Amazon Simple Email Service (Amazon SES)** - *An easy-to-use,
    cost-effective email solution for applications.*
-   **Amazon Simple Notification Service (Amazon SNS)** - *A web service
    that enables applications, end-users, and devices to instantly send
    and receive notifications from the cloud.*
-   **Amazon Simple Queue Service (Amazon SQS)** - *Reliable and
    scalable hosted queues for storing messages as they travel between
    computers.*
-   **Amazon Simple Workflow Service (Amazon SWF)** - *A fully managed
    service that helps developers build, run, and scale background jobs
    that have parallel or sequential steps. Amazon SWF is like a state
    tracker and task coordinator in the cloud.*
-   **Amazon Virtual Private Cloud (Amazon VPC)** - *A web service for
    provisioning a logically isolated section of the AWS cloud where you
    can launch AWS resources in a virtual network that you define. You
    control your virtual networking environment, including selection of
    your own IP address range, creation of subnets, and configuration of
    route tables and network gateways.*
-   **Amazon WorkSpaces** - *A managed, secure desktop computing service
    for provisioning cloud-based desktops and providing users access to
    documents, applications, and resources from supported devices.*
-   **AWS Auto Scaling** - *A fully managed service that enables you to
    quickly discover the scalable AWS resources that are part of your
    application and configure dynamic scaling.*
-   **AWS CloudTrail** - *A web service that records AWS API calls for
    your account and delivers log files to you. The recorded information
    includes the identity of the API caller, the time of the API call,
    the source IP address of the API caller, the request parameters, and
    the response elements returned by the AWS service.*
-   **AWS Direct Connect** - *A web service that simplifies establishing
    a dedicated network connection from your premises to AWS. Using AWS
    Direct Connect, you can establish private connectivity between AWS
    and your data center, office, or colocation environment.*
-   **AWS Elastic Beanstalk** - *A web service for deploying and
    managing applications in the AWS Cloud without worrying about the
    infrastructure that runs those applications.*
-   **AWS Identity and Access Management (IAM)** - *A web service that
    enables Amazon Web Services (AWS) customers to manage users and user
    permissions within AWS.*
-   **AWS Lambda** - *A web service that lets you run code without
    provisioning or managing servers. You can run code for virtually any
    type of application or back-end service with zero administration.
    You can set up your code to automatically trigger from other AWS
    services or call it directly from any web or mobile app.*
-   **AWS OpsWorks** - *A configuration management service that helps
    you use Chef to configure and operate groups of instances and
    applications. You can define the application's architecture and the
    specification of each component including package installation,
    software configuration, and resources such as storage. You can
    automate tasks based on time, load, lifecycle events, and more.*
-   **AWS Snowball** - *A petabyte-scale data transport solution that
    uses devices designed to be secure to transfer large amounts of data
    into and out of the AWS Cloud.*
-   **AWS Storage Gateway** - *A web service that connects an
    on-premises software appliance with cloud-based storage to provide
    seamless and secure integration between an organization's
    on-premises IT environment and AWS's storage infrastructure.*
-   **AWS Trusted Advisor** - *A web service that inspects your AWS
    environment and makes recommendations for saving money, improving
    system availability and performance, and helping to close security
    gaps.*
-   **AWS X-Ray** - *A web service that collects data about requests
    that your application serves, and provides tools you can use to
    view, filter, and gain insights into that data to identify issues
    and opportunities for optimization.* \#\# IAM - Identity Access
    Management

IAM allows you to manage users and their level of access to the AWS
Console. It's a global AWS Service. It offers following features:

-   Centralized control of AWS account
-   Shared access to AWS account
-   Granular permissions
-   Identity federation (including AD, Facebook, LinkedIn, etc..)
-   Multifactor Authentication
-   Provide temporary access for users / devices and services where
    neccessary
-   Allows to set up own password rotation policy
-   Supports PCI DSS Compliance (For taking Credit Card details)

**Root account** is the account that you first setup the AWS account. It
has complete admin access. When creating new users, **they have no
permissions by default**. New users are assigned **Access Key ID** and
**Secret Access Key** when first created. Those keys can be used to
programmatically access the ecosystem not to login in to the AWS
console. Access keys are not the same as a password. **They can be only
viewed once**, so if they are lost, new keys must be generated.

### Users

Simply end users such as people, employees of an organization etc...

### Groups

A collection of users. Each user in the group will inherit the
permissions of the group

### Policies (=Permissions)

Policies are made up of documents, called **Policy documents**. These
documents are in a format called JSON and **they give permissions as to
what a user / group / role is able to do**.

### Roles

Roles can be assigned to AWS resources\#\# AWS Object Storage and CDN -
S3, Glacier and CloudFront

### S3 - Simple Storage Service

S3 provides developers and IT teams with secure, durable,
highly-scalable object storage. Amazon S3 is easy to use, with a simple
web services interface to store and retrieve any amount of data from
anywhere on the web. So it is actually following:

-   Safe place to store files
-   Object-based storage
-   Data is spread across multiple devices and facilities
-   Files can be from 0 bytes to 5 terabytes.
-   Unlimited storage (pay by the gigs)
-   Files are stored in buckets (folder in the cloud)
-   Bucket names are universal so they have to be unique (each bucket
    has an DNS address)

S3 **is build for 99.99% availability** and **Amazon guarantees 99.9%**.
Amazon also guarantees **99.999999999% durability** for information
inside of S3 (11 x 9s). S3 also offers for a lifecycle management which
can be used to move files after certain age to another storage tier (f.e
cheaper one, glacier). It offers versioning and encryption for files.
Data can be secured using access control lists and bucket policies.
Bucket policies operate on bucket level and access control lists goes
down to a single file (object).

Example bucket url:

    https://s3.eu-west-1.amazonaws.com/something

    https://s3.[region].amazonaws.com/[bucket-name]

When uploading files to S3, HTTP 200 status code is returned if upload
has been successful.

**By default all buckets are private and all objets stored inside them
are private**

#### S3 Objects

S3 is object-based and it's objects consist of following:

-   key (name of the object)
-   value (data of the object and is made up of sequence of bytes)
-   version id (used for versioning)
-   metadata (data about stored data)
-   subresources (access control lists, torrent)

#### Data Consistency Model

S3 offers **read after write** consistency for **PUST of new objects**
and **eventual consistency** for **overwrite PUTS and DELETES**. So it
can take some time to propagate.

#### Storage Tiers

-   **S3 Standard**: *99.99% availability, 99.999999999% durability,
    stored redundantly across multiple devices (disks) in multiple
    facilities and is designed to sustain the loss of two facilities
    (AZ) concurrently.*
-   **S3 - IA (Infrequently Accessed**: *For data that is accessed less
    frequently, but requires rapid access when needed. Lower fee than
    s3, but you are charged a retrieval fee*
-   **S3 One Zone - IA**: *same as S3 IA but even lower cost. If
    Multi-AZ resilience is not needed, this one is a good choice*
-   **S3 RRS - Reduced Redundancy Storage**: *What is this?*
-   **Glacier** - *Very cheap storage, but used for archival only.
    Expedited, standard or bulk. A standard retrieval time takes 3 - 5
    hours.*

#### Encryption

S3 offers Client Side Encryption and Server Side Encryption.

Server side encryption can be following:

-   **SSE-S3**: *Amazon S3 Managed Keys*
-   **SSE-KMS**: *KMS*
-   **SSE-C**: *Customer Provided Keys (own keys)*

#### Pricing

Pricing is based on:

-   Storage
-   Requests
-   Storage Management Pricing (tags)
-   Data Transfer Pricing (cross region replication)
-   Transfer Acceleration (fast, easy and secure transfer of files over
    long distances, CloudFront)

#### Versioning

Once versioning has been enabled for a bucket, it can't be removed (only
suspended). Only way to do that is by deleting the bucket. When deleting
a object from bucket where versioning has been enabled, it doesn't
delete the actual object it only marks it as deleted (soft delete).
Versioning has a MFA delete capability, which provides additional layer
of security.

#### Cross Region Replication

-   Versioning must be enabled on both the source and destination
    buckets.
-   Regions must be unique
-   Files in an existing bucket are not replicated automatically. All
    subsequent updated files will be replicated automatically.
-   You cannot replicate to multiple buckets or use daisy chaining.
-   Delete markers are not replicated
-   Deleting individual versions or deletem markers will not be
    replicated

#### Lifecycle Management

-   Can be used with or without versioning
-   Can be applied to current versions and previous versions
-   Following actions can be done: -- Transition to the S3 Standard to
    S3 IA (30 days after creation) -- Archive to the Glacier (30 days
    after IA) -- Permanently delete

### CloudFront

A Content Delivery Network (CDN) is a system of distributed servers
(network) that deliver webpages and other web content to a user based on
the geographic locations of the user, the origin of the webpage and a
content delivery server.

Amazon CloudFront can be used to deliver entire website, including
dynamic, static, streaming, and interactive content using a global
network of edge locations. Requests for your content are automatically
routed to the nearest edge location, so content is delivered with the
best possible performance. It is optimized to work with other AWS
services like S3, EC2, ELB, Route53. CloudFront also works seamlessly
with any non-AWS origin server wich stores the original, definitive
versions of your files.

#### Terminology

-   **Edge Location**: *This is the location where content will be
    cached. This is separate to an AWS region / AZ. **Edge locations are
    not READONLY***
-   **Origin**: *This is the origin of all files that the CDN will
    distribute. This can be either an S3 bucket, an EC2 instance, an
    ELB, or Route53*
-   **Distribution**: *This is the name given the CDN which consists of
    a collection of Edge Locations* -- **Web Distribution**: *Used for
    websites* -- **RTMP**: *Used for Media Streaming*

### Storage Gateway

AWS Storage Gateway is a service that connects an on-premises software
appliance with cloud-based storage to provide seamless and secure
integration between an organization's on-premises IT environment and
AWS's storage infrastructure. The service enables you to securely store
data to the AWS cloud for scalable and cost-effective storage.

Storage Gateway software appliance is available for download as virtual
machine (VM) image that you install on a host in your datacenter.
Storage Gateway supports either VMware ESXi or Microsoft Hyper-V. Once
Gateway is installed and accociated with AWS account through the
activation process, it can be used via AWS Management Console to create
storage gateway option.

There are four types of Storage Gateways

-   File Gateway (NFS)
-   Volumes Gateway (iSCSI) -- Stored Volumes -- Cached Volumes
-   Tape Gateway (VTL)

#### File Gateway

Files are stored as objects in your S3 buckets, accessed through a
Network File System (NFS) mount point. Ownership, permissionsm and
timestamps are durably stored in S3 in the user-metadata of the object
associated with the file. Once objects are transferred to S3, they can
be managed as native S3 objects, and bucket policies such as versioning,
lifecycle management, and cross-region replication apply directly to
objects stored in bucket.

#### Volume Gateway

The volume interface presents applications with disk volumes using the
iSCSI block protocol. Data written to these volumes can be
asynchronously backed up as point-in-time snapshots of your volumes, and
stored in the cloud as Amazon EBS snapshots. Snapshots are incremental
backups that capture only changed blocks. All snapshot storage is also
compressed to minimize your storage charges.

##### Stored Volumes

Stored Volumes let store primary data locally, while asynchronously
backing up the data to AWS. Stored Volumes provide on-premises
applications with low-latency access to entire datasets, while providing
durable, off-site backups. Data written to stored volumes is
asynchronously backed up to S3 in the form of EBS snapshots. 1GB - 16TB
in size for Stored Volumes.

##### Cached Volumes

Cached Volumes let use S3 as primary data storage while retaining
frequently accessed data locally in Storage Gateway. Cached Volumes
minimize the need to scale on-premises storage infrastructure, while
still providing applications with low-latency access to frequently
accessed data. Storage Volumes can be created up to 32 TB in size and
attached to iSCSI devices from on-premises application servers. Gateway
stores written data in Storage Gateway cache and uploads buffer storage.
1GB - 32 TB for Cached Volumes.

#### Tabe Gateway

Tape Gateway offers a durable, cost-effective solution to archive data
in the AWS Cloud. The VTL interface lets leverage existing tape-based
backup application infrastructure to store data on virtual tape
cartridges that is created on the Tape Gateway. Each Tape Gateway is
preconfigured with a media changeer and tape drives, which are available
to existing client backup applications as iSCSI devices. Supported By
NetBackup, Backup Exec, Veeam, etc..

### Snowball

AWS Import/Export Disk accelerates moving large amounts of data into and
out of the AWS cloud using portable storage devices for transport. This
is old service which is now superceded by snowball. There are three
types of snowballs:

-   Snowball
-   Snowball Edge
-   Snowmobile

Snowball can import to and from S3.

##### Snowball

Snowball is a petabyte-scale data transport solution that uses secure
appliances to transfer large amounts of data into and out of AWS. Using
Snowball addresses common challenges with large-scale data transferrs
including high network costs, long transfer times, and security
concerns. Transferring data with Snowball is simple, fast, secure and
can be as little as one-fifth the cost of high-speed internet.

80TB snowball in all regions. Snowball uses multiple layers of security
designed to protect your data including tamper-resistan enclosures,
256-bit encryption, and an industry-standard Trusted Platform Module
(TPM) designed to ensure both security and full chain-of-custody of your
data. Once the data transfer job has been processed and verified, AWS
performs a software erasure of the snowball appliance.

##### Snowball Edge

AWS Snowball Edge is a 100TB data transfer device with on-board storage
and compute capabilities. Snowball Edge can be used to move large
amounts of data into and out of AWS, as temporary storage tier for large
local datasets, or to support local workloads in remote or offline
locations.

Snowball Edge connects to existing applications and infrastructure using
standard storage interfaces, streamlining the data transfer process and
minimizing setup and integration. Snowball Edge can cluster together to
form a local storage tier and process your data on-premises, helping
ensure your applications continue to run even when they are not able to
access the cloud.

##### Snowmobile

AWS Snowmobile is an Exabyte-scale data transfer service used to move
extremely large amounts of data to AWS. 100 PB can be transferred per
snowmobile.

### S3 Transfer Acceleration

S3 Transfer Acceleration utilises the CloudFront Edge Network to
accelerate uploads to S3. Instead of uploading them directly to S3
bucket a distinct url can be used to upload it to edge location which
then transfers the uploaded file to S3.

    https://[name].s3-accelerate.amazonaws.com

EC2 - The Backbone of AWS
-------------------------

Amazon Elastic Compute Cloud is a web service that provides resizeable
compute capacity in the cloud. EC2 reduces the time required to obtain
and boot new server instances to minutes, allowing to quickly scale
capacity, both up and down, as computing requirements change.

EC2 changes the economics of computing by allowing to pay only for
capacity that is actually in use. EC2 provides developers the tools to
build failure resilient applications and isolate themselves from common
failure scenarios.

### Pricing

-   **On Demand**: *allows to pay fixed rate by the hour (or by the
    second) with no commitment*
-   **Reserved**: *provides capacity reservation and offers significant
    discount on the hourly charge for an instance 1 yr or 3 yr terms.*
-   **Spot**: *enables to bid whatever price wanted for an instance,
    providing even greater savings if application has flexible start and
    end times. If spot instance is terminated by Amazon, that hour will
    not be charged. If instance is terminated manually, then the
    complite hour is charged*
-   **Dedicated Hosts**: *Physical EC2 server dedicated for only private
    use, reduces costs by allowing to use existing server-bound software
    licenses*

### EBS - Elastic Block Storage

Amazon EBS allows to create storage volumes and attach them to EC2
instances. Once attached, file system can be created on top of these
volumes, run database, or use them any other way as any regular block
device. EBS volumes are placed in a specific AZ, where they are
automatically replicated to protect from failure of a single component

-   **General Purpose SSD (GP2)**: *General purpose, balances both price
    and performance, ratio of 3 IOPS per GB with up to 10 000 IOPS and
    the ability to burst up to 3 000 IOPS for extended periods of time
    for volumes at 3334 Gb and above.*
-   **Provisioned IOPS SSD (IO1)**: *Designed for I/O intensive
    applications such as large relational or NoSQL databases. Should be
    used if need more than 10 000 IOPS. Can provision up to 20 000 IOPS
    per volume*
-   **Throughput Optimized HDD (ST1)**: *Big data, data warehouses, log
    proccessing, cannot be a boot volume*
-   **Cold HDD (SC1)**: *Lowest cost storage for infrequenlty accessed
    workloads, f.e File Server, cannot be a boot volume*
-   **Magnetic (Standard)**: *Lowest cost per gigabyte for all EBS
    volume types that is bootable. Magnetic volumes are ideal for
    workloads where data is accessed infrequently, and applications
    where the lowest storage cost is important, is bootable*

### Security Groups

-   All inbound traffic is blocked by default
-   All outbound traffic is allowed by default
-   Changes to Security Groups take effect immediately
-   You can have any number of EC2 instances within a security group
-   You can have multiple security groups attached to EC2 instances
-   Security Group are **STATEFUL** -- if you create an inbound rule
    allowing traffic in, that traffic is automatically allowed back out
    again
-   you can't block specific IP addresses using Security Groups, instead
    use NACL
-   You can specify allow rules, but not deny rules

### Volumes & Snapshots

-   Volumes exists on EBS (Virtual Hard Disk)
-   Snapshots exists on S3
-   Snapshots are point in time copies of volumes
-   Snapshots are incremental (meaning that only the blocks that have
    changed since last snapshot are moved to S3)
-   First snapshots may take some tiem to create
-   Instance must be stopped before taking a snapshot of root device
    volume, however snapshot can be taken while the instance is running
-   AMI's can be created from EBS backed instances and snapshots
-   EBS volume size can be changed on the fly and storage type
-   Volumes will ALWAYS be in the same availability zone as the EC2
    instance
-   To move an EC2 volume from one AZ/region, take snapshot or image of
    it and then copy to new AZ/region
-   Snapshots of encrypted volumes are encrypted automatically
-   Volumes restored from encrypted snapshots are encrypted
    automatically
-   You can share snapshots, but only if they are unencrypted -- These
    snapshots can be shared with other AWS accounts or made public

### RAID, Volumes & Snapshots

-   RAID (Redundant Array of Independent Disks) -- RAID 0 - Striped, No
    Redundancy, Good Performance -- RAID 1 - Mirrored, Redundancy --
    RAID 5 - Good for reads, bad for writes, AWS doesn't recommend ever
    putting RAID 5's on EBS -- RAID 10 - Striped & Mirrored, Good
    Redundancy, Good Performance

For taking a snapshot of RAID array the following steps are needed:

-   Stop the application from writing to disk
-   Flush all caches to the disk

Can be done by:

-   Freezing the file system
-   Unmounting RAID array
-   Shutting down the associated EC2

### AMI's - EBS Root Volumes vs Instance Store

AMI's can be based on:

-   Region
-   OS
-   Architecture (32-bit or 64-bit)
-   Launch Permissions
-   Storage for Root Device -- Instance Store (Ephemeral Storage) -- EBS
    Backed Volumes

All AMIs are categorized as either backed by Amazon EBS or backed by
Instance Store.

For EBS Volumes: the root device for an instance launched from AMI is an
Amazon EBS volume created from an Amazon EBS snapshot

For Instance Store Volumes: the root device for an instace launched from
the AMI is an instance store volume created from a tempalte stored in S3

-   Instance Store volumes can't be stopped. If the underlying host
    fails, data is lost.
-   EBS backed instances can be stopped. Data will not be lost if
    instance has been stopped.
-   You can reboot both without loosing data
-   By default, both root volumes will be deleted on termination.
    However with EBS volumes, you can tell AWS to keep the root device
    volume.

### ELB - Elastic Load Balancers

There are three types of load balancers in AWS:

-   Application Load Balancer
-   Network Load Balancer
-   Classic Load Balancer

Application Load Balancers are best suited for HTTP(S) traffic. They
oparate at Layer 7 and are application-aware. They are intelligent, and
advanced request routing can be created.

Network Load Balancers are best suited for TCP traffic where extreme
performance is required. Operating at the connection level (Layer 4),
they are capable of handling millions of requests per second, while
maintaining ultra-low latencies.

Classic Load Balncers are the legacy Elastic Load Balancers. You can
load balance HTTP(S) applications and use Layer 7-specific features,
such as X-Forwarded and sticky sessions. You can also use strict Layer 4
load balancing for applications that rely purely on the TCP protocol

### CloudWatch

-   Standard Monitoring is 5 minutes
-   Detailed Monitoring is 1 minute

CloudWatch allows to create dashboards to see whats happening inside AWS
environment. It allows to set alarms that notify when particular
thresholds are hit. Events helps to respond to changes in AWS resources
and logs helps to aggrecate, monitor and store logs.

### Auto Scaling

-   Before you can create Auto Scaling Group you have to create launch
    configurations
-   When you hit Create Launch Configuration =\> it won't provision any
    EC2 instance
-   Subnet defines which availability zones use auto scaling (normally
    choose all). If you choose multiple instances as a Group Size then
    it disributes all instances evenly across the selected availability
    zones. If you select only one availability zone then it launches all
    instances in that particular availability zone.
-   You can select to receive traffic from Elastic Load Balancer(s)
-   Health Check Grace Period = The length of time that Auto Scaling
    waits before checking an instance's health status. The grace period
    begins when an instance comes into service. Default values is 300
    seconds. Grace period can be removed by setting it to zero (0)
-   Scaling Policies allows you to launch and kill EC2 instances using
    configured alarms.

### EC2 Placement Groups

Placement group name must be unique within you AWS account. Instances
that can be launched in a placement group:

-   Compute Optimized
-   Memory Optimized
-   Storage Optimized
-   GPU

AWS recommends homogenous instances within placement groups (size and
the family should be same). Placement groups can't be merged together.
Existing instances can't be moved into a placement group but AMI can be
created from existing instance and then be launched into a placement
group

#### Clustered Placement Group

A cluster placement group is a grouping of instances within a single
availability zone (can't span across multiple availability zones).
Placement groups are recommended for applications that need low network
latency, high network throughput or both. Only certain instances can be
launched into a clustered placement groups. (f.e. Can't use T2 Micro or
T2 Nano)

#### Spread Placement Group

A spread placement group is a group of instances that are each placed on
distinct underlying hardware. Spread placement groups are recommended
for applications that have a small number of critical instances that
should be kept separate from each other. It's recommended for
applications that have a small number of critical instances that should
be kept seperate from each other (opposite of clustered placement
group). Also it can span across multiple availability zones.

### EFS - Elastic File System

Elastic File System (EFS) is a file storage service for EC2 instances.
It's easy to use and provides a simple interface that allows you to
create and configure file systems quickly and easily. With EFS storage
capacity is elastic, growing and shrinking automatically as you add and
remove files so applications have the sotrage they need, when they need
it

#### Features

-   Supports the Network File System version 4 (NFSv4) protocol
-   Pay for the storage you use (no pre-provisioning)
-   Scales up to the petabytes
-   Supports thousands of concurrent NFS connections
-   Data is stored across multiple availability zones within a region
-   **Block-based storage** not object-base storage
-   Read after write consistency
-   Allows multiple EC2 instances to connect to it (f.e EBS can be
    mounted to single EC2 instance)

### Lambda

AWS Lambda is a compute service where you can upload your code and
create a lambda function. AWS Lambda takes care of provisioning and
managing the servers that you use to run the code. You don't have to
worry about operating systems, patching, scaling, etc... Lambda events
can trigger other lambda events. Everytime you do something with lambda
that is just one instance of lambda running. Architectures can get
complicated so AWS X-ray helps you to debug the stack. Lambda is
serverless since there is no need to manage any instances.

Currently supported languages are:

-   Node.js
-   Java
-   Python
-   C\#

#### Pricing

Pricing is based on number of requests or duration. First one million
requests are free and then it costs \$0.20 per one million requests.
Duration is calculated from the time when code begings its execution to
the point where it returns or is terminated. That time is rounded to the
nearest 100ms. Duration based pricing depends on how much memory is
allocated to the function. (\$0.00001667 for every GB-second used).
Maximum threshold for duration is five minutes. If function doesn't
execute in that threshold, you have to break it to multiple functions
and make first one trigger next one.

#### Event-driven compute service

Runs your core in response to events. These events could be changes to
data in S3 bucket or DynamoDB table. Currently available triggers:

-   API Gateway
-   AWS IoT
-   Alexa Skills Kit
-   Alexa Smart Home
-   CloudFront
-   CloudWatch Events
-   CloudWatch Logs
-   CodeCommit
-   Cognito Sync Trigger
-   DynamoDB
-   Kinesis
-   S3
-   SNS

#### Compute service

Runs your code in response to HTTP(s) requests using API Gateway or API
calls made using AWS SDKs.

### EC2 Summary

Termination Protection is turned off by default so you must turn it on
manually. When deleting EBS-backed EC2 instance, the default action is
for the root EBS volume to be deleted when the instance is terminated.
EBS-backed root + additional volumes can be encrypted using AWS API or
console or you can use 3rd party tool (bitlocker).

#### Volumes vs Snapshots

Volumes exists on EBS and they are basically just a virtual hard disks
in the cloud and snapshots exists on S3. Snapshots can be taken of a
volume and stored to S3. Those snapshots are point in time copies of
volumes. Snapshots are also incremental so only changed (changed since
last snapshot) blocks are moved to S3. First snapshot can take time to
create depending on size of the volume. Snapshots of encrypted volumes
are encrypted automatically and restored snapshots are also encrypted
automatically. Snapshots can be shared with other AWS accounts or made
public but only if they are unencrypted.

#### EBS vs Instance Store

Instance Store Volumes are sometimes called Ephemeral Storage. Instance
Store Volume can't be stopped so if the host fails data will be lost
with EBS-backed instances can be stopped without losing data. Both can
be rebooted without loosing data. By default both ROOT volumes will be
deleted on termination but EBS can be configured to keep the root device
volume.

Snapshots of RAID array can be tricky due to interdependencies of the
array so the solution here is to take application consistent snapshot
which includes following steps:

-   Stop the application from writing to disk.
-   Flush all caches to disk

How to do:

-   Freeze the file system
-   Unmount the RAID array
-   Shutdown the EC2 instance

#### Amazon Machine Images

Amazon Machine Images are regional which means that it can be launched
from the region in which it's stored. However AMI can be copied to other
regions using the AWS console, CLI or EC2 API

#### Monitoring

-   Standard Monitoring = every 5 minutes
-   Detailed Monitoring = every 1 minute (costs extra)

##### CloudTrail

CloudTrail is for auditing and can be turned on to record every actions
in AWS account.

##### CloudWatch

CloudWatch is for performance monitoring and with it custom dashboards
can be created to see whats happening inside your AWS environment. It
also allows to create alarms which will notify when particular
thresholds are hit (f.e CPU utilization over 80%). CloudWatch can also
be used to create events that respond to state changes in AWS resources.
CloudWatch also provides logs which are not AWS specific meaning that it
can handle 3rd party logs from outside of AWS.

#### Roles

Roles are more secure than storing access key and secret access key on
individual EC2 instances. F.e if the EC2 intance is compromised then
attackers have both keys and can use them to access AWS resources. They
are also easier to manage. Roles can be assigned to EC2 instance
**after** it has been provisioned using CLI or AWS console. Roles are
universal and can be used in **any region**

#### Instance Meta-data

Used to get information about an instance and it's address is always
same http://169.254.169.254/latest/meta-data/ there is also user-data
service located in http://169.254.169.254/latest/user-data/

#### EFS

EFS supports NFSv4 protocol and can scale up to petabytes. It can suppor
thousands of concurrent NFS connections. Data is stored accross
availability zones.

#### Lambda

#### Placement Groups

Two types of Placement Groups

-   Clustered Placement Group
-   Spread Placement Group

#### EC2 pricing models:

-   **On Demand**, where you pay by the second or by the hour
-   **Spot**, where you set a bid price and if that spot price equals
    that bid price the instance will be provisioned. Also if spot price
    goes higher than bid price the instance will be terminated. Anything
    less than 1 hour and you won't be charged but if you manually
    terminate that instance you will be charged
-   **Reserved**, where you reserve capacity. Contracts run from 12
    months to 36 months. More you pay upfront the bigger discount you
    get.
-   **Dedicated Hosts**, where you won't be sharing any hardware with
    others

#### EC2 instance types:

-   **F1**, Field Programmable Gate Array, *Genomics research, financial
    analytics, realtime video processing, big data, etc..*
-   **I3**, High Speed Storage, *NoSQL, DBs, Data Warehousing, etc..*
-   **G3**, Graphics Intensive, *Video Encoding / 3D Application
    Streaming*
-   **H1**, High Disk Throughput, *MapReduce-based workloads,
    distributed file systems (HDSF, MapR-FS)*
-   **T2**, Lowest Cost, General Purpose, *Web Servers / Small DBs*
-   **D2**, Dense Storage, *Fileservers / Data Warehousing / Hadoop*
-   **R4**, Memory Optimized, *Memory Intensive Apps / DBs*
-   **M5**, General Purpose, *Application Servers*
-   **C5**, Compute Optimized, *CPU Intensive Apps / DBs*
-   **P3**, Graphics/General Purpose GPU, *Machine Learning, Bitcoin
    mining, etc...*
-   **X1**, Memory Optimized, *SAP HANA / Apache Spark etc...*

Remember: **FIGHTDRMCPX**

#### EBS types

-   **SSD**, General Purpose - GP2 - (Up to 10 000 IOPS)
-   **SSD**, Provisioned IOPS, IO1 - (More than 10 000 IOPS)
-   **HDD**, Throughput Optimized - ST1 - frequently accessed workloads
-   **HDD**, Cold - SC1 - less frequently accessed data (f.e file
    server)
-   **HDD**, Magnetic - Standard - cheap, infrequently accessed storage

EC2 boot volumes can be GP2, IO1 or HDD Magnetic. EBS Volume can't be
mounted to multiple EC2 instances; instead use EFS or S3.

The Well Achitected Framework
-----------------------------

### Business Benefits of Cloud

-   Almost zero upfront infrastructure investment
-   Just-in-time Infrastructure
-   More efficient resource utilization
-   Usage-based costing
-   Reduced time to market

### Technical Benefits of Cloud

-   Automation - "Scriptable infrastructure"
-   Auto-scaling
-   Proactive scaling
-   More efficient development lifecycle
-   Improved testability
-   Disaster recovery and business continuity
-   "Overflow" the traffic to the cloud

### Design For Failure

**Rule of thumb**: Be pessimist when designing architectures in the
cloud; asume things will fail. In other words, always design, implement
and deploy for automated recovery from failure.

In particular, asume that your **hardware will fail**. Asume that
**outages will occur**. Asume that some **disaster will strike your
application**. Asume that you will be slammed with **more than the
expected number of request** per second some day. Asume that with time
your **application software will fail too**. By being pessimist, you end
up thinking about recovery strategies during design time, which helps in
designing an overall system better.

### Decouple Components

The key is to build components that do not have tight dependencies on
each other, so that if one component were to die (fail), sleep (not
responding) or remain busy (slow to respond) for some reason, the other
components in the system are built so as to continue to work as if no
failure is happening

In essence, loose coupling isolates the various layers and components of
application so that each component interacts asynchronously with the
others and treats them as a "black box".

For example, in the case of web application architecture, app server can
be isolated from the web server and from the database. The app server
does not know about the web server and vice versa, this gives decoupling
between these layers and there are no dependencies code-wise or
functional perspectives.

In the case of batch-processing architechture, independent asynchronous
components can be created.

### Implement Elasticity

The cloud brings a new concept of elasticity to applications. Elasticity
can be implemented in three ways:

-   **Proactive cyclic scaling**: *Periodic scaling that occurs at fixed
    interval (daily, weekly, monthly, quarterly)*
-   **Proactive event-based scaling**: *Scaling just when expecting a
    big surge of traffic request due to scheduled business event (new
    product launch, marketing campaings, black friday)*
-   **Auto-scaling based on demand**: *By using monitoring service,
    system can send triggers to take appropriate actions so that it
    scales up or down based on metrics (CPU utilization, network I/O,
    etc...)*

### Pillar One - Security

#### Design Priciples

-   Apply security at all layers
-   Enable traceability
-   Automate responses to security events
-   Focus on securing the system
-   Automate security best practices (harden the OS)

#### Definition

Security in the cloud consists of four areas;

-   Data Protection
-   Privilege Management
-   Infrastructure Protection
-   Detective Controls

Before you begin to architect security practices across your
environment, basic data classification should be in place. You should
organise and classify your data in to segments such as publicly
available, available to only members of your organization, available to
only certain members of your organization, available only to the board,
etc... You should also implement a least privilege access system so that
people are only able to access what they need. However most importantly,
you should encrypt everything where possible, whether it be at rest or
in transit.

Privilege management ensures that only authorized and authenticated
users are able to access resources, and only in a manner that is
intended. It can include:

-   Access Control Lists (ACLs)
-   Role Based Access Controls
-   Password Management (password rotation policies)

Detective controls can be used to **detect or identify a security
breach**. AWS services to achieve this include:

-   AWS CloudTrail
-   Amazon CloudWatch
-   AWS Config
-   Amazon Simple Storage Service (S3)
-   Amazon Glazier

#### Key AWS Services

-   **Data Protection**: *Encrypt data in transit and at rest using;
    ELB, EBS, S3 & RDS*
-   **Privilege Management**: *IAM, MFA*
-   **Infrastructure Protection**: *VPC*
-   **Detective Controls**: *CloudWatch, CloudTrail, AWS Config*

### Pillar Two - Reliability

The reliability pillar covers the ability of a system to recover from
service or infrastructure outages/distruptions as well as the ability to
dynamically acquire computing resources to meet demand.

#### Design Principles

-   Test recovery procedures
-   Automatically recover from failure
-   Scale horizontally to increase aggregate system availability
-   Stop guessing capacity

#### Definition

Reliability in the cloud consist of three areas;

-   Foundations
-   Change Management
-   Failure Management

Before building a house, you always make sure that the foundation are in
place before you lay the first brick. Similarly before architecting any
system, you need to make sure you have the prerequisite foundations. In
traditional on-premises IT one of the first things you should consider
is the size of the comms link between you HQ and you datacenter. If you
missprovision this link, it can take 3 - 6 months to upgrade which can
cause huge distruption to your traditional IT estate.

With AWS, they handle most of the foundation for you. The cloud is
designed to be essentially limitless meaning that AWS handle the
networking and compute requirements themselves. However they do set
service limits to stop customers from accidentally over-provisioning
resources.

You need to be aware of how change affects a system so that you can plan
proactively around it. Monitorin allows you to detect any changes to
your environment and react. In traditional on-premises systems, change
control is done manually and are carefully co-ordinated with auditing.

With AWS things are a lot easier, you can use CloudWatch to monitor your
environment and services such as autoscaling to automate change in
response to changeas on you production environment.

With cloud, you should always architect your systems with the assumption
that failure will occur. You should become aware of these failures, how
they occurred, how to respond to them and then plan on how to prevent
these from happening again.

#### Key AWS Services

-   **Foundations**: *IAM, VPC*
-   **Change Management**: *AWS CloudTrail*
-   **Failure Management**: *AWS CloudFormation*

### Pillar Three - Performance Efficiency

The Performance Efficiency pillar focuses on how to use computing
resources efficiently to meet your requirements and how to maintain that
efficiency as demand changes and technology evolves.

#### Design Principles

-   Democratize advanced technologies
-   Go global in minutes
-   Use serverless architectures
-   Experiment more often

#### Definition

Performance Efficiency in the cloud consists of four areas;

-   Compute
-   Storage
-   Database
-   Space-time trade-off

When architecting your system it is importan to choose right kind of
server. Some applications require heavy CPU utilization, some require
heavy memory utilization, etc... With AWS servers are virtualized and at
the click of a button (or API call) you can change the type of server in
which your environment is running on. You can even switch to running
with no servers at all and use AWS lambda

The optimal storage solutions for your environment depends on a number
of factors. For example;

-   **Accress Method**: *Block, File or Object*
-   **Patterns of Access**: *Random or Sequential*
-   **Throughput Required**
-   **Frequency of Access**: *Online, Offline, Archival*
-   **Frequency of Update**: *Worm, Dynamic*
-   **Availability Constraints**
-   **Durability Constraints**

At AWS the storage is virtualized. With S3 you can have 99.999999999%
durability, cross region replication, etc.. With EBS you can choose
between different storage mediums (such as SSD, Magnetic, PIOPS etc..).
You can also easily move volumes between the different types of storage
mediums.

The optimal database solution depends on a number of factors. Do you
need database consistency, do you need high availability, do you need
NoSQL, do you need DR etc? With AWS you get a lot of options. RDS,
DynamoDB, Redshift etc..

Using AWS you can use services such as RDS to add read replicas,
reducing the load on you database and creating multiple copies of the
database. This helps lower latency. You can use Direct Connect to
provide predictable latency between your HQ and AWS. You can use global
infrastructure to have multiple copies of your environment, in regions
that is closest to you customer base. You can also use caching services
such as ElastiCache or CloudFront to recude latency.

#### Key AWS Services

-   **Compute**: *Autoscaling*
-   **Storage**: *EBS, S3, Glacier*
-   **Database**: *RDS, DynamoDB, Redshift*
-   **Space-Time Trade-Off**: *CloudFront, ElastiCache, Direct Connect,
    RDS Read Replicas*

### Pillar Four - Cost Optimization

Use the Cost Optimization pillar to reduce your costs to a minimum and
use those savings for other parts of your business. A cost-optimized
system allows you to pay the lowest price possible while still achieving
your business objectives.

#### Design Principles

-   Transparency attribute expenditure
-   Use managed services to reduce cost of ownership
-   Trade capital expense for operating expense
-   Benefit from economies of scale
-   Stop spending money on data center operations

#### Definition

Cost Optimization in the cloud consists of four areas;

-   Matched supply and demand
-   Cost-effective resources
-   Expenditure awareness
-   Optimizing over time

Try to optimally align supply with demand. Don't over provision or under
provision, instead as demand grows, so should your supply of compute
resources. Think of things like autoscaling which scale with demand.
Similarly in a serverless context, use services such as lambda that only
execute (or respond) when a request (demand) comes in. Services such as
CloudWatch can also help you keep track as to what your demand is.

Using the correct instance type can be key to cost savings. F.e you
might have a reporting process that is running on a t2-micro and it
takes 7 hours to complete. That same process could be run on an
m4.2xlarge in a manner of minutes. The result remains the same but the
t2.micro is more expensive because it ran for longer. A well architected
system will use the most cost efficient resources to reach the end
business goal.

With cloud you no longer have to go out and get quotes on physical
servers, choose a supplier, have those resources delivered, installed,
made available etc.. You can provision things within seconds, however
this comes with its own issues. Many organisations have different teams,
each with their own AWS accounts. Being aware of what each team is
spending and where is crucial to any well architected system. You can
use cost allocation tags to track this, billing alerts as well as
consolidated billing.

AWS moves FAST. There are hundreds of new services (and potentially 1000
new services this year). A service that you chose yesterday may not be
the best service to be using today. F.e consider MySQL RDS, Aurora was
launched at re:Invent 2014 and is now out of preview. Aurora may be
better option now for your business because of its performance and
redundancy. You should keep track of the changes made to AWS and
constantly re-evaluate your existing architecture. You can do this by
subscribing to the AWS blog and by using services such as Trusted
Advisor

#### Key AWS Services

-   **Mached supply and demand:** *Autoscaling*
-   **Cost-effective resources**: *EC2 (reserved instances), AWS Trusted
    Advisor*
-   **Expenditure awareness**: *CloudWatch Alarms, SNS*
-   **Optimizing over time**: *AWS Blog, AWS Trusted Advisor*

### Pillar Five - Operational Excellence

The Operational Excellence pillar includes operational practices and
procedures used to manage production workloads. This includes how
planned changes are executed, as well as responses to unexpected
operational events.

Change execution and responses should be automated. All processes and
procedures of operational excellence should be documented, tested and
reqularly reviewed.

#### Design Principles

-   Perform operatios with code
-   Align operation process to business objectives
-   Make regular, small, incremental changes
-   Test for responses to unexpected events
-   Learn from operational events and failures
-   Keep operations procedures current

#### Definition

There are three best practice areas for Operational Excellence in the
cloud:

-   Preparation
-   Operation
-   Response

##### Preparation

Effective preparation is required to drive operational excellence.
Operations checklist will ensure that workloads are ready for production
operation, and prevent unintentional production promotion without
effective preparation. Workloads should have

-   **Runbooks**: *operations guidance that operations teams can reffer
    to so they can perform n ormal daily tasks*
-   **Playbooks**: *guidance for responding to unexpected operational
    events. Playbooks should include response plans, as well escalation
    paths and stakeholder notifications*

In AWS are several methods, services and features that can be used to
support operational readiness, and the ability to prepare for normal
day-to-day operations as well as unexpected operational events.

**CloudFormation** can be used to ensure that environments contain all
required resources when deployed in production, and that the
configuration of the environment is based on tested best practices,
which reduces the opportunity for human error.

Implementing **AutoScaling**, or other automated scaling mechanism, will
allow workloads to automatically respond when business-related events
affect operational needs. Services like **AWS Config** with AWS Config
rules feature create mechnanisms to automatically track and respond to
changes in you AWS workloads and environments.

It is also important to use features like **tagging** to make sure all
resources in a workload can be easily identified when needed during
operations and responses.

Be sure that documentation does not become stale or out of date as
procedures change. Also make sure that it is thorough. Without
application designs, environment configurations, resource
configurations, response plans, and mitigation plans, documentation is
not complete. If documentation is not updated and tested regularly, it
will not be useful when unexpected operational events occur. If
workloads are not reviewed before production, operations will be
affected when undetected issues occur. If resources are not documented,
when operational events occur, determining how to respond will be more
difficult while the correct resources are identified.

##### Operation

Operations should be standardized and mangeable on a routine basis. They
focus should be on automation, small frequent changes, regular quality
assurance testing, and defined mechanisms to track, audit, roll back,
and review changes. Changes should not be large and infrequent, they
should not require scheduled downtime, and they should not require
manual execution. A wide range of logs and metrics that are based on key
operational indicators for a workload should be collected and reviewed
to ensure continuous operations.

In AWS you can set up a continuous integration / continuous deployment
(CI/CD) pipeline (e.g. source code repository, build systems, deployment
and testing automation). Release management processes, whether manual or
automated, should be tested and be based on small incremental changes
and tracked versions. You should be able to revert changes t hat
introduce operational issues without causing operational impact.

Routine operations, as well as responses to unplanned events, should be
automated. Manual processes for deployments, release management,
changes, and rollbacks should be avoided. Releases should not be large
batches that are done infrequentlyt. Rollbacks are more difficult in
large changes, and failing to have rollback plan, or the ability to
mitigate failure impacts, will prevent continuity of operations. Align
monitoring to business needs, so that the responses are effective at
maintaining business continuity. Monitoring that is ad hoc and not
centralized, with responses that are manual, will cause more impact to
operations during unexpected events.

##### Response

Responses to unexpected operational events should be automated. This is
not just for alerting, but also for mitigation, remediation, rollback,
and recovery. Alerts should be timely, and should invoke escalations
when responses are not adequate to mitigate the impact of operational
events. Quality assurance mechanisms should be in place to automatically
roll back failed deployments. Responses should follow a pre-defined
playbook that includes stakeholders, the escalation process, and
procedures. Escalation paths should be defined and include both
functional and hierarchical escalation capabilities. Hierachical
escalation should be automated, and escalated priority should result in
stakeholder notifications

In AWS there are serveral mechanisms to ensure both appropriate alerting
and notification in reponse to unplanned operational events, as well as
automated responses

#### Key AWS Services

-   **Preparation**: *AWS Config provides a detailed inventory of your
    AWS resources and configuration, and continuously records
    configuration changes. AWS Service Catalog helps to create a
    standardized set of service offerings that are aligned to best
    practices. Designing workloads that use automation with services
    like AutoScaling, and Amazon SQS, are good methods to ensure
    continuous operations in the event of unexpected operational
    events.*
-   **Operations**: *AWS CodeCommit, AWS CodeDeploy and AWS CodePipeline
    can be used to manage and automate code changes to AWS workloads.
    Use AWS SDKs or third-party libraries to automate operational
    changes. Use AWS CloudTrail to audit and track changes made to AWS
    environments*
-   **Responses**: *Take advantage of all the Amazon CloudWatch service
    features for effective and automated responses. Amazon CloudWatch
    alarms can be used to set thresholds for alerting and notification,
    and Amazon CloudWatch events can trigger notifications and automated
    responses*\#\# Route53

Route53 is global service which contains six different routing policies

-   Simple Routing Policy
-   Weighted Routing Policy
-   Latency Routing Policy
-   Failover Routing Policy
-   Geolocation Routing Policy
-   Multivalue Answer Routing Policy

There are 50 domain names available by default, however it is a soft
limit that can be raised by contacting AWS support.

### Simple Routing Policy

Simple Routing Policy can have only one record with multiple IP
addresses. If you specify multiple values in a record, it will return
values in a random order.

### Weighted Routing Policy

Weighted Routing Policy can split traffic accross multiple IP addresses
with predefined weight f.e 25% traffic to eu-west-1 and 75% traffic to
eu-west-2. Weight can be a number which are added together and divided
with weight. F.e if you give first record 25, second 170 and third 1000,
the first ones weight is calculated with 25 / 25+170+1000 = 0.021%.

### Latency Routing Policy

Latency based routing allows to route traffic based on the lowest
network latency for the end user. (f.e which region gives them the
fastest response time)

### Failover Routing Policy

Failover Routing Policies are used to create active/passive set up. f.e
if the primary site is located in eu-west-2 and secondary site is in
ap-southeast-2 then the Route53 can be configured to monitor the primary
site using health check. If primary site fails the health check, all the
traffic will be redirected to secondary site located in ap-southeast-2.

### Geolocation Routing Policy

Geolocation Routing Policy routes traffic depending on end users
locations which DNS queries originates. f.e european customers can be
configured to connect eu-west-1 and US customers to us-east-1

### Multivalue Answer Routing Policy

Multivalue Answer Routing Policy allows to route traffic approximately
randomly to multiple resource f.e EC2 instances. Multivalue answer
record can be created for each resource and optionally assiociate with
an health check with each record. F.e suppose you manage an HTTP web
service with a dozen web servers that each have their own IP address. No
one web server could handle all of the traffic but if you create a dozen
multivalue answer records then Amazon Route53 responds to DNS queries
with up to eight healty records in response to each DNS query. Route53
gives different answers to different DNS resolvers. If one server
becomes unavailable after resolver caches a response, client software
can try another IP address in the response

SQS - Simple Queue Service
--------------------------

Amazon SQS is a web service that gives you access to a message queue
that can be used to store messages while waiting for a computer to
process them. Amazon SQS is a distributed queue system that enables web
service applications to quickly and reliably queue messages that one
component in the application generates to be consumed by another
component. A queue is a temporary repository for messages that are
awaiting processing.

SQS can decouble the components of an application so they run
independently, easing message management between components. Any
component of a distributed application can store messages in the queue.
Message can contain up to 256Kb of text in any format. Any component can
later retrieve the messages programmatically using the SQS API.

The queue acts as a buffer between components producing and saving data,
and the component receiving the data for processing. This means the
queue resolves issues that arise if the producer is producing work
faster than the consumer can process it, or if the producer or consumer
are only intermittently connected to the network.

### Queue Types

There are two types of Queue:

-   Standart Queues (default)
-   FIFO Queues (First-In-First-Out)

#### Standard Queues

SQS offers standard as the default queue type. A standard queue can have
nearly-unlimited number of transactions per second. Standard Queues
quarantee that a message is delivered at least once. However,
occasionally (because of highly-distributed architecture that allows
high throughput), more than one copy of a message might be delivered out
of order. Standard queues provide best-effort ordering which ensures
that messages are generally delivered in the same order as they are
sent.

#### FIFO Queues

The FIFO queue complements the standard queue. The most important
features of this queue type are FIFO delivery and exactly one
processing: The order in which messages are sent and received is
strictly preserved and a message is delivered once and remains available
until a consumer processes and deletes it; dublicates are not introduced
into the queue. FIFO queues also support message groups that allow
multiple ordered message groups within a single queue. FIFO queues are
limited to 300 transactions per second (TPS). but have all the
capabilities of standard queues.

#### SQS Facts

-   SQS is pull-based, not pushed-based system.
-   Messages are max. 256Kb in size
-   Messages can be kept in the queue from 1 minute to 14 days
-   Default retention period is 4 days
-   SQS guarantees that your messages will be processed at least once

SQS Visibility timeout is the amount of time that the message is
invisible in the SQS queue after a reader picks up that message.
Provided the job is processed before the visibility timeout expires, the
message will then be deleted from the queue. If the job is not processed
within that time, the message will become visible again and another
reader will process it. This could result in the same message being
delivered twice. Default visibility timout is 30 seconds and should be
increased if task takes more than that. Maximum is 12 hours.

SQS Long polling is a way to retrieve messages from your SQS queues.
Reqular short polling returns immediately (even if the message queue
being polled is empty), long polling doesn't return a response until
message arrives in the message queue, or the long poll times out so long
polling can save money.

SWF - Simple Workflow Service
-----------------------------

Amazon Simple Workflow Service is a web service that makes it easy to
coordinate work across distributed application components. SWF enables
applications for a range of use cases, including media processing, web
application back-ends, business process workflows, and analytics
pipelines, to be designed as coordination of tasks.

Tasks represent invocations of various processing steps in an
application which can be performed by executablee code, web service
calls, human actions, and scripts.

### Workers

Workers are programs that interact with SWF to get tasks, process
received tasks, and return the results.

### Decider

Decider is a program that control the coordination of tasks, i.e their
ordering, concurrency and scheduling according to the application logic.

The workers and the deciders can run on cloud infrastructure, such as
EC2, or on machines behind firewalls. SWF brokers the interactions
between workers and the decider. It allows the decider to get consistent
views into the progress of tasks and to initiate new tasks in an
outgoing manner.

At the same time, SWF stores tasks, assigns them to workers when they
are ready, and monitors their progress. It ensures that a task is
assigned only once and is never duplicated. Since SWF maintains the
application's state durably, workers and deciders don't have to keep
track of execution state. They can run independently, and scale quickly.

### SWF Domains

Workflow and activity types and the workflow execution itself are all
scoped to a domain. Domains isolate a set of types, executions, and task
lists from other within the same account.

Domain can be registered using the AWS Management Console or by using
the RegisterDomain action in the SWF API.

Maximum workflow can be 1 year and the value is always measured in
seconds.

SQS vs SWF
----------

-   SWF presents a task-oriented API, whereas SQS offers a
    message-oriented API.
-   SWF ensures that a task is assigned only once and is never
    duplicated. SQS need duplicate message handling and it can process
    messages twice.
-   SWF keeps track of all the tasks and events in an application. With
    SQS application level tracking needs to be implemented especially if
    application uses multiple queues.

SNS - Simple Notification Service
---------------------------------

Amazon Simple Notification Service is a web service that makes it easy
to set up, operate, and sent notifications from the cloud. It provides
developers with highly scalable, flexible, and cost-effective capability
to publish messages from an application and immediately deliver them to
subscribers or other applications.

Besides pushing cloud notifications directly to mobile devices, SNS can
also deliver notifications by SMS text message or email, to SQS queues,
or to any HTTP endpoint. SNS notifications can also trigger Lambda
functions. When a message is published to an SNS topic that has a Lambda
function subscribed to it, the lambda function isinvoked with the
payload of the published message. The lambda function receives the
message payload as an input parameter and can manipulate the information
in the message, publish the message to other SNS topics, or send the
message to other AWS service.

SNS allows to group multiple recipients using topics. A topic is an
"access point" for allowing recipients to dynamically subscribe for
identical copies of the same notification. One topic can support
deliveries to multiple endpoint types. F.e iOS, android and SMS
recipients can be groupped together and when publishing once to a topic,
SNS delivers appropriately formatted copies of message to each
subscriber.

### SNS Benefits

-   Instantaneous, push-based delivery (no polling)
-   Simple APIs and easy integration with applications
-   Flexible message delivery over multiple transport protocols
-   Inexpensive, pay-as-you-go model with no up-front costs
-   Web-based AWS Management Console offers the simplicity of a
    point-and-click interface

### Pricing

Users pay \$0.50 per 1 million SNS Requests and after that it's:

-   \$0.06 per 100k SNS over HTTP
-   \$0.75 per 100 SNS over SMS
-   \$2.00 per 100k SNS over EMAIL

SNS vs SQS
----------

-   Both messaging services in AWS
-   SNS Push
-   SQS Polls

Elastic Transcoder
------------------

Elastic Transcoder is a Media Transcoder in the cloud. It allows to
convert media files from their original source format in to different
formats that will play on smartphones, tablets, PC's etc... It provides
transcoding presets for popular output formats, which means that you
don't need to guess about which settings work best on particular
devices. Pricing is based on the minutes that transcoder is using and
the resolution at which is transcoded.

API Gateway
-----------

Amazon API Gateway is fully managed service that makes it easy for
developers to publish, maintain, monitor, and secure APIs at any scale.
With a few clicks in the AWS Magement Console, API that acts as "front
door" for applications can be created to access data, business logic, or
functionality from back-end services such as applications running on
EC2, lambda or any web application.

API Gateway supports enpoint's response caching. With that the number of
calls made to endpoint can be reduced. This affects positively to the
latency. When caching is enabled, API Gateway caches responses from the
enpoint fo a specific TLL (time-to-live) period in seconds. API Gateway
then responds to the request by looking up the endpoint response from
the cache instead of making request to the endpoint.

### Benefits

-   Low cost & efficient
-   Scales effortlessly
-   Can throttle requests to prevent attacks
-   Can connect to CloudWatch to log all requests

Kinesis
-------

Amazon Kinesis is a platform on AWS to send streaming data. Kinesis
makes it easy to load and analyze streaming data, and also providing the
ability for to build own custom applications for business needs. There
are three core services in Kinesis:

-   Kinesis Streams
-   Kinesis Firehose
-   Kinesis Analytics

### Kinesis Streams

Kinesis Streams consists of shards which can do 5 transactions per
second for reads up to maximum total data read rate of 2Mb per second
and up to 1000 records per second for writes, up to a maximum total data
write rate of 1Mb per second (including partition keys). The data
capacity of streams is a function of the number of shards that is
specified to the stream. The total capacity of the stream is the sum of
capacities of it's shard.

### Kinesis Firehose

Kinesis Firehose is completely automated so there is no need to worry
about data consumers, shards, etc... So when the data comes to Firehose
it's either analyzed using lambda or it is sent directly to S3.

### Kinesis Analytics

Kinesis Analytics is a way of analyzing the data inside Kinesis

\#\# Databases on AWS

### RDS - Backups, Multi-AZ & Read Replicas

Two different types of backups; Automated Backups and Database Snapshots

#### Automated Backups

These allows to recover database to any point in time within a
"retention period". This period can be between one and 35 days (1-35).
Automated Backups will take a full daily snapshots and will also store
transaction logs throughout the day. **In case of a recovery AWS will
first choose the most recent daily backup and then apply transaction
logs relevant to that day. ** This allows you to do a point in time
recovery down to a second within the redention period.

Automated Backups are **enabled by default**. The backup data is stored
in S3 and free storage space equal the size of your database is
complimentary. So if you have an RDS instance of 10Gb then 10Gb is
offered free of charge for backups. Backups are taken within defined
window. During the backup window, storage I/O may be suspended while
your data is being backed up and you may experience elevated latency.

**When the RDS instance is deleted, backups accosiated to that instance
will be also deleted.**

#### Database Snapshots

These are done manually by user. **They are stored even after you delete
the RDS instance** unlike automated backups. Think of these as a
standalone files.

#### Restoring Backups

Whenever RDS is restored from backups, the restored version of the
database will be new RDS instance with a new DNS endpoint. Like this:

    original.eu-west-1.rds.amazonaws.com => restored.eu-west-1.rds.amazonaws.com

#### Encryption

Encryption at rest is supported for MySQL, Oracle, SQL Server,
PostgreSQL, MariaDB and Aurora. Encryption is done using the AWS Key
Management Service (KMS). Once your RDS instance is encrypted, the data
stored at the rest in the underlying storage is encrypted, as are its
automated backups, read replicas, and snapshots.

Currently encrypting an existing DB instance is not supported. To use
Amazon RDS encryption for an existing database, one must first create a
snapshot, make a copy of that snapshot and encrypt the copy.

#### Multi-AZ

Multi-AZ allows to have an exact copy of database in another
Availability Zone. AWS handles the replication so when primary database
is written to, write will be automatically synchronized to standby
database. In the event of planned database maintenance, database failure
or even AZ failure, Amazon RDS will automatically failover to the
standby database.

**Multi-AZ is for disaster recovery only and not for improving
performance.** If database is located in eu-west-1 then it's replica can
be located for example in eu-west-2. If eu-west-1 somehow fails, Amazon
will detect this and update DNS address to point eu-west-2. **Never deal
with IP addresses with RDS, use DNS enpoints only.**

Available for:

-   SQL Server
-   Oracle
-   MySQL Server
-   PostgreSQL
-   MariaDB
-   Aurora

#### Read Replica

Read Replicas allows to have read-only copy of a database. This is
achieved by **using async replication** from the primary RDS instance to
the read replica(s). **Read Replicas are used for very read-heavy
database workloads** (Scale out).

Available for:

-   MySQL Server
-   PostgreSQL
-   MariaDB
-   Aurora

Bullets:

-   **Used for scaling and not rod disaster recovery**.
-   Also automatic backups **must be turned on** in order to deploy a
    Read Replica.
-   Max. five (5) Read Replicas are allowed.
-   Read Replicas can have Read Replicas but this will have some
    latency.
-   Each Read Replica will have its **own DNS endpoint**.
-   Read Replicas that have Multi-AZ are allowed. Multi-AZ source
    databases allows to create Read Replicas.
-   Read Replicas can be promoted to be their own databases but this
    will break the replication.
-   Read Replica can be located in another region.

### DynamoDB

Amazon DynamoDB is a fast and flexible NoSQL database service for all
applications that need consistent, single-digit millisecond latency at
any scale. It's fully managed database and supports both document and
key-value data models. It's flexible data model and reliable performance
make it a great fit for mobile, web, gaming, ad-tech, IoT, and many
other applications.

Facts:

-   Stored on SSD storage
-   Spread across three geographically distinct data centres (three
    multiple AZs).
-   Eventual Consistent Reads (By default) - *Consistency across all
    copies of data is usually reached within a second. Repeating a read
    after a short time should return the updated data (**Best read
    performance**)*
-   Strongly Consistent Reads - *A strongly consistent read returns a
    result that reflects all writes that received a succesful response
    prior to read*

#### Pricing

**Pricing uses provisioned throughput capacity** which means that it
costs \$0.0065 per hour for every 10 units of write throughput and
$0.0065 per hour for every 50 units of read throughput. There is also a storage cost of 0.25$
per Gb per month.

How to calculate throughtput:

F.e application performs 1 million writes and 1 million reads per day
while storing 3Gb of data. First calculate how many writes and reads per
second is needed. 1 million evenly spread writes per day is equivalen
to:

    Seconds in a day: 24 (hours) / 60 (minutes) / 60 (seconds) = 86400
    Writes per second: 1 000 000 (writes) /  86400 s = 11.6 w/s

A DynamoDB Write Capacity Unit can handle 1 write per second, so in this
example 12 WCUs are needed. Also with those same numbers 12 Read
Capacity Units are needed. With RCUs billing is calculated blocks of 50
and WCUs blocks of 10.

    WCU: ($0.0065 / 10) x 12 x 24 = $0.187
    RCU: ($0.0065 / 50) x 12 x 24 = $0.0374
    Storage: $0.25 * 3 Gb = $0.75
    Total: $0.9744

### RedShift

Amazon RedShift is a fast and powerful, fully managed, petabyt-scale
data warehouse service in the cloud. Customers can start small for just
\$0.25 per hour with no commitments or upfront costs and scale to a
petabyte or more for \$1000 per terabyte per year, less than tenth of
most other data warehousing solutions.

Instead of storing data as a series of rows, Amazon Redshift organizes
the data by column. Unlike row-based systems, which are ideal for
transaction processing, column-based systems are ideal for data
warehousing and analytics, where queries often involve aggregates
performed over large data sets. Since only the columns involved in the
queries are processed and columnar data is stored sequentially on the
storage media, column-based systems require far fewer I/Os, greatly
improving query performance.

MPP (Massively Parallel Processing) is used when Multi-Node
configurated. Redshift automatically distributes data and query load
across all nodes.

#### Configuration

Starts out with a single node that can have up to 160 Gb worth of data.
Scaling is done via Multi-Node configuration. This type of configuration
consists of a Leader Node (which manages client connections and receives
queries) and Compute Nodes (which store data and perform queries and
computations). Max. number of Compute Nodes is 128.

#### Pricing

Pricing is based on Compute Node Hours (total number of hours you run
across all your compute nodes for the billing period. You are billed for
1 unit per node per hour, so 3-node data warehouse cluster running
persistenly for an entire month would incure 2160 instance hours. Leader
Node is free of charge). Backups and data transfer (only within a VPC,
not outside it) are also chargeable

#### Security

Everything that's communicated to redshift is encrypted in transit using
SSL and rest is encrypted using AES-256. By default RedShift takes care
of Key Management but custom management is available through HSM.

#### Availability

Currently available only in 1 AZ but snapshots can be restored to new AZ
in the event of an outage.

### Elasticache

Elasticache is a web service that makes it easy to deploy, operate, and
scale an in-memory cache in the cloud. The service improves the
performance of web application by allowing you to retrieve information
from fast, managed, in-memory caches, instead of relying entirely on
slower disk-based databases. Used to significantly improve latency and
throughput for many read-heavy application workloads (such as social
networking, gaming, media sharing and Q&A portals) or compute intensive
workloads (such as recommendation engine)

Caching improves application performance by storing critical pieces of
data in memory for low-latency access. Cached informatioin may include
the results of I/O-intensive database queries or the result of
computationally-intensive calculations. There are two types of
elasticache; memcached and redis

#### Memcached

Widely adopted memory object caching system. Elasticache is protocol
compliant with memcached, so popular tools that is used will work
seamlessly with the service

#### Redis

Popular open-source in-memory key-value store that supports data
structures such as sorted sets and lists. Elasticache supports master /
slave replication and Multi-AZ which can be used to achieve cross AZ
redundancy

### Aurora

Amazon Aurora is MySQL-compatible, relational database engine that
combines the speed and availability of high-end commercial databases
with the simplicity and cost-effectiveness of open source databases.
Amazon aurora provides up to five times beter performance than MySQL at
a price point one tenth that of commercial database while delivering
similar performance and availability.

#### Scaling

Starts with 10Gb database that scales in 10Gb increments and storage
autoscales so no need to worry about provisioning more storage. Compute
resources can scale up to 32vCPUs and 244Gb of memory. Scaling does not
have downtime so no need for maintenance window. Aurora maintains two
copies of your data that is contained in each AZ with minimum of 3 AZ.
This means 6 copies of data. It handles loss of two copies of data
without affecting database write availability and up to three copies of
data without affecting read availability so this makes it really highly
redundant. Aurora storage is also self-healing which means that data
blocks and disks are continuosly scanned for errors and repaired
automatically.

#### Replicas

There are two types of replicas currently available; Aurora Replicas,
MySQL Read Replicas. Aurora Replicas are separate Aurora database from
original database and database can have up to 15 of those. MySQL Read
Replicas are the same as normally and database can have up to 5 of
those. **Failover will automatically occur to Aurora Replicas but not
for MySQL Read Replicas**. \#\# VPC (Virtual Private Cloud)

Think of a VPC as a virtual data centre in the cloud. Every region in
the world has a default VPC. Amazon VPC lets you provision a logically
isolated section of the AWS Cloud where you can launch AWS resources in
a virtual network that you define. You have complete control over your
virtual networking environment, including selection of your own IP
adress range, creation of subnets, and configuration of route tables and
network gateways. VPC network configurations are easily customizeable.
F.e public-facing subnet can be created for webservers that have access
to the Internet and private-facing subnet for backend systems such as
databases or application servers with no Internet access. Multiple
layers of security can be leveraged using VPCs (including security
groups, network access control lists)

Hardware Virtual Private Network (VPN) connection between corporate
datacenter and VPC can be created and leverage the AWS cloud as an
extension of your corporate datacenter.

**VPCs always use 10.0.0.0 network address range.**

There are two ways to connect into a VPC; Internet Gateway and Virtual
Private Gateway. Internet Gateway provides Internet access and Virtual
Private Gateway is used when connecting via VPN. Then all the traffic is
sent to a router which uses routing table and then it goes to Network
ACL (Access Control List). After that it either hits to public or
private subnets.

\*\* One subnet equals one Availability Zone\*\*

When creating a VPC it will always generate a Security Group, a Network
ACL, default route but it won't create any subnets.

The first four IP addresses and the last IP address in each subnet CIDR
block are not available to use and thus cannot be assigned to an
instance. F.e in a subnet with CIDR block 10.0.0.0/24, the following
five IP addresses are reserved:

-   **10.0.0.0:** *Network address*
-   **10.0.0.1**: *Reserved by AWS for the VPC Router*
-   **10.0.0.2**: *Reserved by AWS. The IP address of the DNS server is
    always the base of the VPC Network range plus two.*
-   **10.0.0.3**: *Reserved by AWS for future use*
-   **10.0.0.255**: *Network broadcast address. AWS does not support
    broadcast in a VPC, therefore they reserve this adress*

### What to do with a VPC?

-   Instances can be launched into a chosen subnet
-   Custom IP address ranges can be configured to each subnet.
-   Route tables can be configured between subnets *(which subnets are
    allowed to communicate with each other)*
-   Create an Internet Gateway and attach it to a VPC, **only on per
    VPC**
-   Much better security control over AWS resources *(subnets can block
    specific IP addresses, Network ACL can block specific IP addresses,
    instances can be moved to private subnets , security groups can be
    attached to them)*

### Default VPC vs Custom VPC

Default VPC is user friendly, allowing to immediately deploy instances
and all subnets have a route to the Internet. Each EC2 instances will
have both public and private IP adress when deployed to default VPC.
With custom VPC instances will have private IP address only

### VPC Peering

Allows to connect one VPC with another VPC via a direct network route
using private IP adresses. They will behave like they are in a same
private network when you peer VPCs. Multiple AWS accounts can be peer
using VPCs and they behave like they are in the same account. **VPC
doesn't have transitive peering** meaning that if VPC A is peered with
VPC B and VPC C then VPC B and VPC C **can not **communicate without
peering them together

### NAT Instances

When creating a NAT Instance source / destination check on the instance
**must be disabled**. To provide Internet access to private subnets NAT
Instances must be in a public subnet then there must be a route out of
the private subnet to the NAT instance in order for this to work. The
amount of traffic that NAT instance can support depends on the size of
the instance. So if there are any bottlenecks with NAT instance,
increase the size of the instance. High availability can be created
using Autoscaling Groups, multiple subnets in different AZ and a script
to automate failover. NAT instances are always behind a Security Group.

### NAT Gateways

**NAT Gateways are preferred** by businesses and enteprice customers
because they scale automatically up to 10 Gb/s and there is no need to
install patches, worry about security groups and no need to install
antivirus software. They are managed by Amazon and are automatically
assigned a public IP address. Remember to update route tables to point
NAT Gateways. Use NAT Gateways in multiple AZ to have some redundancy in
terms of availability. There is no need to disable source / destination
checks. They are more secure than NAT Instances because they don't have
ssh access.

### Network Access Control Lists

Just like Security Groups, Network Access Control Lists (NACL) **can
only be deployed into one VPC**. By default everything is denied on both
inbound and outbound rules when you create a NACL. When adding inbound /
outbound rules, Amazon recomends to start the rules with 100 IPv4 and
then 101 for IPv6 and then go up in increments of 100. NACL is stateless
so both inbound and outbound rules have to be allowed seperately (unlike
Security Groups which are stateless). When creating VPCs it will
automatically create a default NACL and by default it allows all
outbound and inbound traffic. Each subnet in VPC must be associated with
a NACL and by default its default NACL. NACL contains numbered list of
rules that is evaluated in order, starting with the lowest number.
Specific IP addresses can be blocked with NACL not Security Groups.

### Security Groups

Nothing here.

### VPC Flow Logs

VPC Flow Logs is a feature that enables to capture information about the
IP traffic going to and from your network interfaces in your VPC. Flow
log data is stored using Amazon CloudWatch Logs. Flow Logs can be
created at three levels:

-   VPC
-   Subnet
-   Network Interface Level

Flow Logs can not be enabled for VPC that are peered with VPC unless the
peer VPC is in same account. Flow Logs cannot be tagged. Configuration
is not editable after creation. f.e IAM role cannot be changed.

Not all IP traffic is monitored:

-   Traffic generated by instances when they contact the Amazon DNS
    server. But if own DNS server is used, then all traffic is logged.
-   Traffic genearated by Windows instance for Amazon Windows license
    activation
-   Traffic to and from 169.254.169.254 for instance metadata
-   DHCP traffic
-   Traffic to reserved IP address for the default VPC router

### NATs vs Bastions

A NAT is used to provide internet traffic to EC2 instances in private
subnets and bastion is used to securely administer EC2 instances using
SSH or RDP in private subnets
