#!/bin/bash

pandoc -f markdown -t html <(find . -mindepth 2 -name '*.md' -exec cat {} +) -o book.html
pandoc -f markdown -t epub <(find . -mindepth 2 -name '*.md' -exec cat {} +) -o book.epub --metadata-file metadata.yml
pandoc -f markdown -t markdown <(find . -mindepth 2 -name '*.md' -exec cat {} +) -o README.md
pandoc -f markdown -t html <(find . -mindepth 2 -name '*.md' -exec cat {} +) -o book.pdf --metadata-file metadata.yml
