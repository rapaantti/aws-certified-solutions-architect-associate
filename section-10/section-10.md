## The Well Achitected Framework

### Business Benefits of Cloud

- Almost zero upfront infrastructure investment
- Just-in-time Infrastructure
- More efficient resource utilization
- Usage-based costing
- Reduced time to market

### Technical Benefits of Cloud

- Automation - "Scriptable infrastructure"
- Auto-scaling
- Proactive scaling
- More efficient development lifecycle
- Improved testability
- Disaster recovery and business continuity
- "Overflow" the traffic to the cloud

### Design For Failure

**Rule of thumb**: Be pessimist when designing architectures in the cloud; asume things will fail. In other words, always design, implement and deploy for automated recovery from failure. 

In particular, asume that your **hardware will fail**. Asume that **outages will occur**. Asume that some **disaster will strike your application**. Asume that you will be slammed with **more than the expected number of request** per second some day. Asume that with time your **application software will fail too**. By being pessimist, you end up thinking about recovery strategies during design time, which helps in designing an overall system better.

### Decouple Components

The key is to build components that do not have tight dependencies on each other, so that if one component were to die (fail), sleep (not  responding) or remain busy (slow to respond) for some reason, the other components in the system are built so as to continue to work as if no failure is happening

In essence, loose coupling isolates the various layers and components of application so that each component interacts asynchronously with the others and treats them as a "black box".

For example, in the case of web application architecture, app server can be isolated from the web server and from the database. The app server does not know about the web server and vice versa, this gives decoupling between these layers and there are no dependencies code-wise or  functional perspectives.

In the case of batch-processing architechture, independent asynchronous components can be created.

### Implement Elasticity

The cloud brings a new concept of elasticity to applications. Elasticity can be implemented in three ways:

- **Proactive cyclic scaling**: *Periodic scaling that occurs at fixed interval (daily, weekly, monthly, quarterly)*
- **Proactive event-based scaling**: *Scaling just when expecting a big surge of traffic request due to scheduled business event (new product launch, marketing campaings, black friday)*
- **Auto-scaling based on demand**: *By using monitoring service, system can send triggers to take appropriate actions so that it scales up or down based on metrics (CPU utilization, network I/O, etc...)*


### Pillar One - Security

#### Design Priciples

- Apply security at all layers
- Enable traceability
- Automate responses to security events
- Focus on securing the system
- Automate security best practices (harden the OS)

#### Definition

Security in the cloud consists of four areas;

- Data Protection
- Privilege Management
- Infrastructure Protection
- Detective Controls

Before you begin to architect security practices across your environment, basic data classification should be in place. You should organise and classify your data in to segments such as publicly available, available to only members of your organization, available to only certain members of your organization, available only to the board, etc... You should also implement a least privilege access system so that people are only able to access what they need. However most importantly, you should encrypt everything where possible, whether it be at rest or in transit.

Privilege management ensures that only authorized and authenticated users are able to access resources, and only in a manner that is intended. It can include:

- Access Control Lists (ACLs)
- Role Based Access Controls
- Password Management (password rotation policies)

Detective controls can be used to **detect or identify a security breach**. AWS services  to achieve this include:

- AWS CloudTrail
- Amazon CloudWatch
- AWS Config
- Amazon Simple Storage Service (S3)
- Amazon Glazier

#### Key AWS Services

- **Data Protection**: *Encrypt data in transit and at rest using; ELB, EBS, S3 & RDS*
- **Privilege Management**: *IAM, MFA*
- **Infrastructure Protection**: *VPC*
- **Detective Controls**: *CloudWatch, CloudTrail, AWS Config*

### Pillar Two - Reliability

The reliability pillar covers the ability of a system to recover from service or infrastructure outages/distruptions as well as the ability to dynamically acquire computing resources to meet demand.

#### Design Principles

- Test recovery procedures
- Automatically recover from failure
- Scale horizontally to increase aggregate system availability
- Stop guessing capacity

#### Definition

Reliability in the cloud consist of three areas;

- Foundations
- Change Management
- Failure Management

Before building a house, you always make sure that the foundation are in place before you lay the first brick. Similarly before architecting any system, you need to make sure you have the prerequisite foundations. In traditional on-premises IT one of the first things you should consider is the size of the comms link between you HQ and you datacenter. If you missprovision this link, it can take 3 - 6 months to upgrade which can cause huge distruption to your traditional IT estate.

With AWS, they handle most of the foundation for you. The cloud is designed to be essentially limitless meaning that AWS handle the networking and compute requirements themselves. However they do set service limits to stop customers from accidentally over-provisioning resources.

You need to be aware of how change affects a system so that you can plan proactively around it. Monitorin allows you to detect any changes to your environment and react. In traditional on-premises systems, change control is done manually and are carefully co-ordinated with auditing. 

With AWS things are a lot easier, you can use CloudWatch to monitor your environment and services such as autoscaling to automate change in response to changeas on you production environment.

With cloud, you should always architect your systems with the assumption that failure will occur. You should become aware of these failures, how they occurred, how to respond to them and then plan on how to prevent these from happening again.

#### Key AWS Services

- **Foundations**: *IAM, VPC*
- **Change Management**: *AWS CloudTrail*
- **Failure Management**: *AWS CloudFormation*

### Pillar Three - Performance Efficiency

The Performance Efficiency pillar focuses on how to use computing resources efficiently to meet your requirements and how to maintain that efficiency as demand changes and technology evolves.

#### Design Principles

- Democratize advanced technologies
- Go global in minutes
- Use serverless architectures
- Experiment more often

#### Definition

Performance Efficiency in the cloud consists of four areas;

- Compute
- Storage
- Database
- Space-time trade-off

When architecting your system it is importan to choose right kind of server. Some applications require heavy CPU utilization, some require heavy memory utilization, etc... With AWS servers are virtualized and at the click of a button (or API call) you can change the type of server in which your environment is running on. You can even switch to running with no servers at all and use AWS lambda 

The optimal storage solutions for your environment depends on a number of factors. For example;

- **Accress Method**: *Block, File or Object*
- **Patterns of Access**: *Random or Sequential*
- **Throughput Required**
- **Frequency of Access**: *Online, Offline, Archival*
- **Frequency of Update**: *Worm, Dynamic*
- **Availability Constraints**
- **Durability Constraints**

At AWS the storage is virtualized. With S3 you can have 99.999999999% durability, cross region replication, etc.. With EBS you can choose between different storage mediums (such as SSD, Magnetic, PIOPS etc..). You can also easily move volumes between the different types of storage mediums.

The optimal database solution depends on a number of factors. Do you need database consistency, do you need high availability, do you need NoSQL, do you need DR etc? With AWS you get a lot of options. RDS, DynamoDB, Redshift etc..

Using AWS you can use services such as RDS to add read replicas, reducing the load on you database and creating multiple copies of the database. This helps lower latency. You can use Direct Connect to provide predictable latency between your HQ and AWS. You can use global infrastructure to have multiple copies of your environment, in regions that is closest to you customer base. You can also use caching services such as ElastiCache or CloudFront to recude latency.

#### Key AWS Services

- **Compute**: *Autoscaling*
- **Storage**: *EBS, S3, Glacier*
- **Database**: *RDS, DynamoDB, Redshift*
- **Space-Time Trade-Off**: *CloudFront, ElastiCache, Direct Connect, RDS Read Replicas*

### Pillar Four - Cost Optimization

Use the Cost Optimization pillar to reduce your costs to a minimum and use those savings for other parts of your business. A cost-optimized system allows you to pay the lowest price possible while still achieving your business objectives.

#### Design Principles

- Transparency attribute expenditure
- Use managed services to reduce cost of ownership
- Trade capital expense for operating expense
- Benefit from economies of scale
- Stop spending money on data center operations

#### Definition

Cost Optimization in the cloud consists of four areas;

- Matched supply and demand
- Cost-effective resources
- Expenditure awareness
- Optimizing over time

Try to optimally align supply with demand. Don't over provision or under provision, instead as demand grows, so should your supply of compute resources. Think of things like autoscaling which scale with demand. Similarly in a serverless context, use services such as lambda that only execute (or respond) when a request (demand) comes in. Services such as CloudWatch can also help you keep track as to what your demand is.

Using the correct instance type can be key to cost savings. F.e you might have a reporting process that is running on a t2-micro and it takes 7 hours to complete. That same process could be run on an m4.2xlarge in a manner of minutes. The result  remains the same but the t2.micro is more expensive because it ran for longer. A well architected system will use the most cost efficient resources to reach the end business goal.

With cloud you no longer have to go out and get quotes on physical servers, choose a supplier, have those resources delivered, installed, made available etc.. You can provision things within seconds, however this comes with its own issues. Many organisations have different teams, each with their own AWS accounts. Being aware of what each team is spending and where is crucial to any well architected system. You can use cost allocation tags to track this, billing alerts as well as consolidated billing.

AWS moves FAST. There are hundreds of new services (and potentially 1000 new services this year). A service that you chose yesterday may not be the best service to be using today. F.e consider MySQL RDS, Aurora was launched at re:Invent 2014 and is now out of preview. Aurora may be better option now for your business because of its performance and redundancy. You should keep track of the changes made to AWS and constantly re-evaluate your existing architecture. You can do this by subscribing to the AWS blog and by using services such as Trusted Advisor

#### Key AWS Services

- **Mached supply and demand:** *Autoscaling*
- **Cost-effective resources**: *EC2 (reserved instances), AWS Trusted Advisor*
- **Expenditure awareness**: *CloudWatch Alarms, SNS*
- **Optimizing over time**: *AWS Blog, AWS Trusted Advisor*

### Pillar Five - Operational Excellence

The Operational Excellence pillar includes operational practices and procedures used to manage production workloads. This includes how planned changes are executed, as well as responses to unexpected operational events.

Change execution and responses should be automated. All processes and procedures of operational excellence should be documented, tested and reqularly reviewed.

#### Design Principles

- Perform operatios with code
- Align operation process to business objectives
- Make regular, small, incremental changes
- Test for responses to unexpected events
- Learn from operational events and failures
- Keep operations procedures current

#### Definition

There are three best practice areas for Operational Excellence in the cloud:

- Preparation
- Operation
- Response

##### Preparation

Effective preparation is required to drive operational excellence. Operations checklist will ensure that workloads are ready for production operation, and prevent unintentional production promotion without effective preparation. Workloads should have

- **Runbooks**:  *operations guidance that operations teams can reffer to so they can perform n ormal daily tasks*
- **Playbooks**: *guidance for responding to unexpected operational events. Playbooks should include response plans, as well escalation paths and stakeholder notifications*

In AWS are several methods, services and features that can be used to support operational readiness, and the ability to prepare for normal day-to-day operations as well as unexpected operational events.

**CloudFormation** can be used to ensure that environments contain all required resources when deployed in production, and that the configuration of the environment is based on tested best practices, which reduces the opportunity for human error.

Implementing **AutoScaling**, or other automated scaling mechanism, will allow workloads to automatically respond when business-related events affect operational needs. Services like **AWS Config** with AWS Config rules feature create mechnanisms to automatically track and respond to changes in you AWS workloads and environments. 

It is also important to use features like **tagging** to make sure all resources in a workload can be easily identified when needed during operations and responses.

Be sure that documentation does not become stale or out of date as procedures change. Also make sure that it is thorough. Without application designs, environment configurations, resource configurations, response plans, and mitigation plans, documentation is not complete. If documentation is not updated and tested regularly, it will not be useful when unexpected operational events  occur. If workloads are not reviewed before production, operations will be affected when undetected issues occur. If resources are not documented, when operational events occur, determining how to respond will be more difficult while the correct resources are identified.

##### Operation

Operations should be standardized and mangeable on a routine basis. They focus should be on automation, small frequent changes, regular quality assurance testing, and defined mechanisms to track, audit, roll back, and review changes. Changes should not be large and infrequent, they should not require scheduled downtime, and they should not require manual execution. A wide range of logs and metrics that are based on key operational indicators for a workload should be collected and reviewed to ensure continuous operations.

In AWS you can set up a continuous integration / continuous deployment (CI/CD) pipeline (e.g. source code repository, build systems, deployment and testing automation). Release management processes, whether manual or automated, should be tested and be based on small incremental changes and tracked versions. You should be able to revert changes t hat introduce operational issues without causing operational impact.

Routine operations, as well as responses to unplanned events, should be automated. Manual processes for deployments, release management, changes, and rollbacks should be avoided. Releases should not be large batches that are done infrequentlyt. Rollbacks are more difficult in large changes, and failing to have rollback plan, or the ability to mitigate failure impacts, will prevent continuity of operations. Align monitoring to business needs, so that the responses are effective at maintaining business continuity. Monitoring that is ad hoc and not centralized, with responses that are manual, will cause more impact to operations during unexpected events.

##### Response

Responses to unexpected operational events should be automated. This is not just for alerting, but also for mitigation, remediation, rollback, and recovery. Alerts should be timely, and should invoke escalations when responses are not adequate to mitigate the impact of operational events. Quality assurance mechanisms should be in place to automatically roll back failed deployments. Responses should follow a pre-defined playbook that  includes stakeholders, the escalation process, and procedures. Escalation paths should be defined and include both functional and hierarchical escalation capabilities. Hierachical  escalation should be automated, and escalated priority should result in stakeholder notifications

In AWS there are serveral mechanisms to ensure both appropriate alerting and notification in reponse to unplanned operational events, as well as automated responses

#### Key AWS Services

- **Preparation**: *AWS Config provides a detailed inventory of your AWS resources and configuration, and continuously records configuration changes. AWS Service Catalog helps to create a standardized set of service offerings that are aligned to best practices. Designing workloads that use automation with services like AutoScaling, and Amazon SQS, are good methods to ensure  continuous operations in the event of unexpected operational events.*
- **Operations**: *AWS CodeCommit, AWS CodeDeploy and AWS CodePipeline can be used to manage and automate code changes to AWS workloads. Use AWS SDKs or third-party libraries to automate operational changes. Use AWS CloudTrail to audit and track changes made to AWS environments*
- **Responses**: *Take advantage of all the Amazon CloudWatch service features for effective and automated responses. Amazon CloudWatch alarms can be used to set thresholds for alerting and notification, and Amazon CloudWatch  events can trigger notifications and automated responses*