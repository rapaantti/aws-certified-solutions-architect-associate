## IAM - Identity Access Management 

IAM allows you to manage users and their level of access to the AWS Console. It's a global AWS Service. It offers following features:

- Centralized control of  AWS account
- Shared access to AWS account
- Granular permissions
- Identity federation (including AD, Facebook, LinkedIn, etc..)
- Multifactor Authentication
- Provide temporary access for users / devices and services where neccessary
- Allows to set up own password rotation policy
- Supports PCI DSS Compliance (For taking Credit Card details)

**Root account** is the account that you first setup the AWS account. It has complete admin access. When creating new users, **they have no permissions by default**. New users are assigned **Access Key ID** and **Secret Access Key** when first created. Those keys can be used to programmatically access the ecosystem not to login in to the AWS console. Access keys are not the same as a password. **They can be only viewed once**, so if they are lost, new keys must be generated.

### Users

Simply end users such as people, employees of an organization etc...

### Groups

A collection of users. Each user in the group will inherit the permissions of the group

### Policies (=Permissions)

Policies are made up of documents, called **Policy documents**. These documents are in a format called JSON and **they give permissions as to what a user / group / role is able to do**.

### Roles

Roles can be assigned to AWS resources