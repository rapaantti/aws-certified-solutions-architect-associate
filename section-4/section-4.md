## AWS Object Storage and CDN - S3, Glacier and CloudFront

### S3 - Simple Storage  Service

S3 provides developers and IT teams with secure, durable, highly-scalable object storage. Amazon S3 is easy to use, with a simple web services interface to store and retrieve any amount of data from anywhere on the  web. So it is actually following:

- Safe place to store files
- Object-based storage
- Data is spread across multiple devices and facilities
- Files can be from 0 bytes to 5 terabytes.
- Unlimited storage (pay by the gigs)
- Files are stored in buckets (folder in the cloud)
- Bucket names are universal so they have to be unique (each bucket has an DNS address)

S3 **is build for 99.99% availability** and **Amazon guarantees 99.9%**. Amazon also guarantees **99.999999999% durability**  for information inside of S3 (11 x 9s).  S3 also offers for a lifecycle management which can be used to move files after certain age to another storage tier (f.e cheaper one, glacier). It offers versioning and encryption for files. Data can be secured using access control lists and bucket policies. Bucket policies operate on bucket level and access control lists goes down to a single file (object).

Example bucket url:

```
https://s3.eu-west-1.amazonaws.com/something

https://s3.[region].amazonaws.com/[bucket-name]
```

When uploading files to S3, HTTP 200 status code is returned if upload has been successful.

**By default all buckets are private and all objets stored inside them are private**

#### S3 Objects

S3 is object-based and it's objects consist of following:

- key (name of the object)
- value (data of the object and is made up of sequence of bytes)
- version id (used for versioning)
- metadata (data about stored data)
- subresources (access control lists, torrent)

#### Data Consistency Model

S3 offers **read after write** consistency for **PUST of new objects** and **eventual consistency** for  **overwrite PUTS and DELETES**. So it can take some time to propagate.

#### Storage Tiers

- **S3 Standard**: *99.99% availability, 99.999999999% durability, stored redundantly across multiple devices (disks) in multiple facilities and is designed to sustain the loss of two facilities (AZ) concurrently.*
- **S3 - IA (Infrequently Accessed**: *For data that is accessed less frequently, but requires rapid access when needed. Lower fee than s3, but you are charged a retrieval fee*
- **S3 One Zone - IA**: *same as S3 IA but even lower cost. If Multi-AZ resilience is not needed, this one is a good choice*
- **S3 RRS - Reduced Redundancy Storage**: *What is this?*
- **Glacier** - *Very cheap storage, but used for archival only. Expedited, standard or bulk. A standard retrieval time takes 3 - 5 hours.*

#### Encryption

S3 offers Client Side Encryption and Server Side Encryption.

Server side encryption can be following:

- **SSE-S3**: *Amazon S3 Managed Keys*
- **SSE-KMS**: *KMS*
- **SSE-C**: *Customer Provided Keys (own keys)*

#### Pricing

Pricing is based on:

- Storage
- Requests
- Storage Management Pricing (tags)
- Data Transfer Pricing (cross region replication)
- Transfer Acceleration (fast, easy and secure transfer of files over long distances, CloudFront)

#### Versioning

Once versioning has been enabled for a bucket, it can't be removed (only suspended). Only way to do that is by deleting the bucket. When deleting a object from bucket where versioning has been enabled, it doesn't delete the actual object it only marks it as deleted (soft delete). Versioning has a MFA delete capability, which provides additional layer of security.

#### Cross Region Replication

- Versioning must be enabled on both the source and destination buckets. 
- Regions must be unique
- Files in an existing bucket are not replicated automatically. All subsequent updated files will be replicated automatically.
- You cannot replicate to multiple buckets or use daisy chaining.
- Delete markers are not replicated
- Deleting individual versions or deletem markers will not be replicated 

#### Lifecycle Management 

- Can be used with or without versioning
- Can be applied to current versions and previous versions
- Following actions can be done:
-- Transition to the S3 Standard to S3 IA (30 days after creation)
-- Archive to the Glacier (30 days after IA)
-- Permanently delete

### CloudFront

A Content Delivery Network (CDN) is a system of distributed servers (network) that deliver webpages and other web content to a user based on the geographic locations of the user, the origin of the webpage and a content delivery server. 

Amazon CloudFront can be used to deliver entire website, including dynamic, static, streaming, and interactive content using a global network of edge locations. Requests for your content are automatically routed to the nearest edge location, so content is delivered with the best possible performance. It is optimized to work with other AWS services like S3, EC2, ELB, Route53. CloudFront also works seamlessly with any non-AWS origin server wich stores the original, definitive versions of your files.

#### Terminology

- **Edge Location**:  *This is the location where content will be cached. This is separate to an AWS region / AZ. **Edge locations are not READONLY***
- **Origin**:  *This is the origin of all files that the CDN will distribute. This can be either an S3 bucket, an EC2 instance, an ELB, or Route53* 
- **Distribution**:  *This is the name given the CDN which consists of a collection of Edge Locations*
-- **Web Distribution**: *Used for websites*
-- **RTMP**: *Used for Media Streaming*

### Storage Gateway

AWS Storage Gateway is a service that connects an on-premises software appliance with cloud-based storage to provide seamless and secure integration between an organization's on-premises IT environment and AWS's storage infrastructure. The service enables you to securely store data to the AWS cloud for scalable and cost-effective storage.

Storage Gateway software appliance is available for download as virtual machine (VM) image that you install on a host in your datacenter. Storage Gateway supports either VMware ESXi or Microsoft Hyper-V. Once Gateway is installed and accociated with AWS account through the activation process, it can be used via AWS Management Console to create storage gateway option.

There are four types of Storage Gateways

- File Gateway (NFS)
- Volumes Gateway (iSCSI)
-- Stored Volumes
-- Cached Volumes
- Tape Gateway (VTL)
 
#### File Gateway

Files are stored as objects in your S3 buckets, accessed through a Network File System (NFS) mount point. Ownership, permissionsm and timestamps are durably stored in S3 in the  user-metadata of the object associated with the file. Once objects are transferred to S3, they can be managed as native S3 objects, and bucket policies such as versioning, lifecycle management, and cross-region replication apply directly to objects stored in bucket.

#### Volume Gateway

The volume interface presents applications with disk volumes using the iSCSI block protocol. Data written to these  volumes can be asynchronously backed up as point-in-time snapshots of your volumes, and stored in the cloud as Amazon EBS snapshots. Snapshots are incremental backups that capture only changed blocks. All snapshot storage is also compressed to minimize your storage charges.

##### Stored Volumes

Stored Volumes let store primary data locally, while asynchronously backing up the data to AWS. Stored Volumes provide on-premises applications with low-latency access to entire datasets, while providing durable, off-site backups. Data written to stored volumes is asynchronously backed up to S3 in the form of EBS snapshots. 1GB - 16TB in size for Stored Volumes.

##### Cached Volumes

Cached Volumes let use S3 as primary data storage while retaining frequently accessed data locally in Storage Gateway. Cached Volumes minimize the need to scale on-premises storage infrastructure, while still providing applications with low-latency access to frequently accessed data. Storage Volumes can be created up to 32 TB in size and attached to iSCSI devices from on-premises application servers. Gateway stores written data in Storage Gateway cache and uploads buffer storage. 1GB - 32 TB for Cached Volumes.

#### Tabe Gateway 

Tape Gateway offers a durable, cost-effective solution to archive data in the AWS Cloud. The VTL interface lets leverage existing tape-based backup application infrastructure to store data on virtual tape cartridges that is created on the Tape Gateway. Each Tape Gateway is preconfigured with a media changeer and tape drives, which are available to existing client backup applications as iSCSI devices. Supported By NetBackup, Backup Exec, Veeam, etc..

### Snowball

AWS Import/Export Disk accelerates moving large amounts of data into and out of the AWS cloud using portable storage devices for transport. This is old service which is now superceded by snowball. There are three types of snowballs:

- Snowball
- Snowball Edge
- Snowmobile

Snowball can import to and from S3.

##### Snowball

Snowball is a petabyte-scale data transport solution that uses secure appliances to transfer large amounts of data into and out of AWS. Using Snowball addresses common challenges with large-scale data transferrs including high network costs, long transfer times, and security concerns. Transferring data with Snowball is simple, fast, secure and can be as little as one-fifth the cost of high-speed internet.

80TB snowball in all regions. Snowball uses multiple layers of security designed to protect your data  including tamper-resistan enclosures, 256-bit encryption, and an industry-standard Trusted Platform Module (TPM) designed to ensure both security and full chain-of-custody of your data. Once the data transfer job has been processed and verified, AWS performs a software erasure of the snowball appliance.

##### Snowball Edge

AWS Snowball Edge is a 100TB data transfer device with on-board storage and compute capabilities. Snowball Edge can be used to move large amounts of data into and out of AWS, as temporary storage tier for large local datasets, or to support local workloads in remote or offline locations.

Snowball Edge connects to existing applications and infrastructure using standard storage interfaces, streamlining the data transfer process and minimizing setup and integration. Snowball Edge can cluster together to form a local storage tier and process your data on-premises, helping ensure your applications continue to run even when they are not able to access the cloud.

##### Snowmobile

AWS Snowmobile is an Exabyte-scale data transfer service used to move extremely large amounts of data to AWS. 100 PB can be transferred per snowmobile. 


### S3 Transfer Acceleration

S3 Transfer Acceleration utilises the CloudFront Edge Network to accelerate uploads to S3. Instead of uploading them directly to S3 bucket a distinct url can be used to upload it to edge location which then transfers the uploaded file to S3.

```
https://[name].s3-accelerate.amazonaws.com
```
