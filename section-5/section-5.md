## EC2 - The Backbone of AWS

Amazon Elastic Compute Cloud is a web service that provides resizeable compute capacity in the cloud. EC2 reduces the time required to obtain and boot new server instances to minutes, allowing to quickly scale capacity, both up and down, as computing requirements change.

EC2 changes the economics of computing by allowing to pay only for capacity that is actually in use. EC2 provides developers the tools to build failure resilient applications and isolate themselves from common failure scenarios.

### Pricing

- **On Demand**: *allows to pay fixed rate by the hour (or by the second) with no commitment*
- **Reserved**: *provides capacity reservation and offers significant discount on the hourly charge for an instance 1 yr or 3 yr terms.*
- **Spot**: *enables to bid whatever price wanted for an instance, providing even greater savings if application has flexible start and end times. If spot instance is terminated by Amazon, that hour will not be charged. If instance is terminated manually, then the complite hour is charged*
- **Dedicated Hosts**: *Physical EC2 server dedicated for only private use, reduces costs by allowing to use existing server-bound software licenses*

### EBS - Elastic Block Storage

Amazon EBS allows to create storage volumes and attach them to EC2 instances. Once attached, file system can be created on top of these volumes, run database, or use them any other way as any regular block device. EBS volumes are placed in a specific AZ, where they are automatically replicated to protect from failure of a single component

- **General Purpose SSD (GP2)**: *General purpose, balances both price and performance, ratio of 3 IOPS per GB with up to 10 000 IOPS and the ability to burst up to 3 000 IOPS for extended periods of time for volumes at 3334 Gb and above.*
- **Provisioned IOPS SSD (IO1)**: *Designed for  I/O intensive applications such as large relational or NoSQL databases. Should be used if need more than 10 000 IOPS. Can provision up to 20 000 IOPS per volume*
- **Throughput Optimized HDD (ST1)**: *Big data, data warehouses, log proccessing, cannot be a boot volume*
- **Cold HDD (SC1)**: *Lowest cost storage for infrequenlty accessed workloads, f.e File Server, cannot be a boot volume*
- **Magnetic (Standard)**: *Lowest cost per gigabyte for all EBS  volume types that is bootable. Magnetic volumes are ideal for workloads where data is accessed infrequently, and applications where the lowest storage cost is important, is bootable*

### Security Groups

- All inbound traffic is blocked by default
- All outbound traffic is allowed by default
- Changes to Security Groups take effect immediately
- You can have any number of EC2 instances within a security group
- You can have multiple security groups attached to EC2 instances
- Security Group are **STATEFUL**
-- if you create an inbound rule allowing traffic in, that traffic is automatically allowed back out again
- you can't block specific IP addresses using Security Groups, instead use NACL
- You can specify allow rules, but not deny rules

### Volumes & Snapshots

- Volumes exists on EBS (Virtual Hard Disk)
- Snapshots exists on S3
- Snapshots are point in time copies of volumes
- Snapshots are incremental (meaning that only the blocks that have changed since last snapshot are moved to S3)
- First snapshots may take some tiem to create
- Instance must be stopped before taking a snapshot of root device volume, however snapshot can be taken while the instance is running
- AMI's can be created from EBS backed instances and snapshots
- EBS volume size can be changed on the fly and storage type
- Volumes will ALWAYS be in the same availability zone as the EC2 instance
- To move an EC2 volume from one AZ/region, take snapshot or image of it and then copy to new AZ/region
- Snapshots of encrypted volumes are encrypted automatically
- Volumes restored from encrypted snapshots are encrypted automatically
- You can share snapshots, but only if they are unencrypted
-- These snapshots can be shared with other AWS accounts or made public

### RAID, Volumes & Snapshots

- RAID (Redundant Array of Independent Disks)
-- RAID 0 - Striped, No Redundancy, Good Performance
-- RAID 1 - Mirrored, Redundancy
-- RAID 5 - Good for reads, bad for writes, AWS doesn't recommend ever putting RAID 5's on EBS
-- RAID 10 - Striped & Mirrored, Good Redundancy, Good Performance

For taking a snapshot of RAID array the following steps are needed:

- Stop the application from writing to disk
- Flush all caches to the disk

Can be done by:

- Freezing the file system
- Unmounting RAID array
- Shutting down the associated EC2

### AMI's - EBS Root Volumes vs Instance Store

AMI's can be based on:

- Region
- OS
- Architecture (32-bit or 64-bit)
- Launch Permissions
- Storage for Root Device
-- Instance Store (Ephemeral Storage)
-- EBS Backed Volumes

All AMIs are categorized as either backed by Amazon EBS or backed by Instance Store. 

For EBS Volumes: the root device for an instance launched from AMI is an Amazon EBS volume created from an Amazon EBS snapshot

For Instance Store Volumes: the root device for an instace launched from the AMI is an instance store volume created from a tempalte stored in S3

- Instance Store volumes can't be stopped. If the underlying host fails, data is lost.
- EBS backed instances can be stopped. Data will not be lost if instance has been stopped.
- You can reboot both without loosing data
- By default, both root volumes will be deleted on termination. However with EBS volumes, you can tell AWS to keep the root device volume.


### ELB - Elastic Load Balancers

There are three types of load balancers in AWS:

- Application Load Balancer
- Network Load Balancer
- Classic Load Balancer

Application Load Balancers are best suited for HTTP(S) traffic. They oparate at Layer 7 and are application-aware. They are intelligent, and advanced request routing can be created.

Network Load Balancers are best suited for TCP traffic where extreme performance is required. Operating at the connection level (Layer 4), they are capable of handling millions of requests per second, while maintaining ultra-low latencies.

Classic Load Balncers are the legacy Elastic Load Balancers. You can load balance HTTP(S) applications and use Layer 7-specific features, such as X-Forwarded and sticky sessions. You can also use strict Layer 4 load balancing for applications that rely purely on the TCP protocol

### CloudWatch 

- Standard Monitoring is 5 minutes
- Detailed Monitoring is 1 minute

CloudWatch allows to create dashboards to see whats happening inside AWS environment. It allows to set alarms that notify when particular thresholds are hit. Events helps to respond to changes in AWS resources and logs helps to aggrecate, monitor and store logs.


### Auto Scaling

- Before you can create Auto Scaling Group you have to create launch configurations
- When you hit Create Launch Configuration => it won't provision any EC2 instance
- Subnet defines which availability zones use auto scaling (normally choose all). If you choose multiple instances as a Group Size then it disributes all instances evenly across the selected availability zones. If you select  only one availability zone then it launches all instances in that particular availability zone.
- You can select to receive traffic from Elastic Load Balancer(s)
- Health Check Grace Period = The length of time that Auto Scaling waits before checking an instance's health status. The grace period begins  when an instance comes into service. Default values is 300 seconds. Grace period can be removed by setting it to zero (0)
- Scaling Policies allows you to launch and kill EC2 instances using configured alarms.

### EC2 Placement Groups

Placement group name must be unique within you AWS account. Instances that can be launched in a placement group:

- Compute Optimized
- Memory Optimized
- Storage Optimized
- GPU

AWS recommends homogenous instances within placement groups (size and the family should be same). Placement groups can't be merged together. Existing instances can't be moved into a placement group but AMI can be created from existing instance and then be launched into a placement group

#### Clustered Placement Group

A cluster placement group is a grouping of instances within a single availability zone (can't span across multiple availability zones). Placement groups are recommended for applications that need low network latency, high network throughput or both. Only certain instances can be launched into a clustered placement groups. (f.e. Can't use T2 Micro or T2 Nano)

#### Spread Placement Group

A spread placement group is a group of instances that are each placed on distinct underlying hardware. Spread placement  groups are recommended for applications that have a small number of critical instances that should be kept separate from each other. It's recommended for applications that have a small number of critical instances that should be kept seperate from each  other (opposite of clustered placement group). Also it can span across multiple availability zones.

### EFS - Elastic File System

Elastic File System (EFS) is a file storage service for EC2 instances. It's easy to use and provides a simple interface that allows  you to create and configure file systems quickly and easily. With EFS storage capacity is elastic, growing and shrinking automatically as you add and remove files so applications have the sotrage they need, when they need it

#### Features

- Supports the Network File System version 4 (NFSv4) protocol
- Pay for the storage you use (no pre-provisioning)
- Scales up to the petabytes
- Supports thousands of concurrent NFS connections 
- Data is stored across multiple availability zones within a region
- **Block-based storage** not object-base storage 
- Read after write consistency
- Allows multiple EC2 instances to connect to it (f.e EBS can be mounted to single EC2 instance)

### Lambda

AWS Lambda is a compute service where you can upload your code and create a lambda function. AWS Lambda takes care of provisioning and managing the servers that you use to run the code. You don't have to worry about operating systems, patching, scaling, etc... Lambda events can trigger other lambda events. Everytime you do something with lambda that is just one instance of lambda running. Architectures can get complicated so AWS X-ray helps you to debug the stack. Lambda is serverless since there is no need to manage any instances.

Currently supported languages are:

- Node.js
- Java
- Python
- C#

#### Pricing

Pricing is based on number of requests or duration. First one million requests are free and then it costs $0.20 per one million requests. Duration is calculated from the time when code begings its execution to the point where it returns or is terminated. That time is rounded to the nearest 100ms. Duration based pricing depends on how much memory is allocated to the function. ($0.00001667 for every GB-second used). Maximum threshold for duration is five minutes. If function doesn't execute in that threshold, you have to break it to multiple functions and make first one trigger next one.

#### Event-driven compute service

Runs your core in response to events. These events could be changes to data in S3 bucket or DynamoDB table. Currently available triggers:

- API Gateway
- AWS IoT
- Alexa Skills Kit
- Alexa Smart Home
- CloudFront
- CloudWatch Events
- CloudWatch Logs
- CodeCommit
- Cognito Sync Trigger
- DynamoDB
- Kinesis 
- S3
- SNS

#### Compute service 

Runs your code in response to HTTP(s) requests using API Gateway or API calls made using AWS SDKs. 


### EC2 Summary

Termination Protection is turned off by default so you must turn it on manually. When deleting EBS-backed EC2 instance, the default action is for the root EBS volume to be deleted when the instance is terminated. EBS-backed root + additional volumes can be encrypted using AWS API or console or you can use 3rd party tool (bitlocker). 

#### Volumes vs Snapshots

Volumes exists on EBS and they are basically just a virtual hard disks in the cloud and snapshots exists on S3. Snapshots can be taken of a volume and stored to S3. Those snapshots are point in time copies of volumes. Snapshots are also incremental so only changed (changed since last snapshot) blocks are moved to S3. First snapshot can take time to create depending on size of the volume. Snapshots of encrypted volumes are encrypted automatically and restored snapshots are also encrypted automatically. Snapshots can be shared with other AWS accounts or made public but only if they are unencrypted. 

#### EBS vs Instance Store

Instance Store Volumes are sometimes called Ephemeral Storage. Instance Store Volume can't be stopped so if the host fails data will be lost with EBS-backed instances can be stopped without losing data. Both can be rebooted without loosing data. By default both ROOT volumes will be deleted on termination but EBS can be configured to keep the root device volume.

Snapshots of RAID array can be tricky due to interdependencies of the array so the solution here is to take application consistent snapshot which includes following steps:

- Stop the application from writing to disk.
- Flush all caches to disk

How to do:

- Freeze the file system
- Unmount the RAID array
- Shutdown the EC2 instance

#### Amazon Machine Images

Amazon Machine Images are regional which means that it can be launched from the region in which it's stored. However AMI can be copied to other regions using the AWS console, CLI or EC2 API

#### Monitoring

- Standard Monitoring = every 5 minutes
- Detailed Monitoring = every 1 minute (costs extra)

##### CloudTrail

CloudTrail is for auditing and can be turned on to record every actions in AWS account.

##### CloudWatch

CloudWatch is for performance monitoring and with it custom dashboards can be created to see whats happening inside your AWS environment. It also allows to create alarms which will notify when particular thresholds are hit (f.e CPU utilization over 80%). CloudWatch can also be used to create events that respond to state changes in AWS resources. CloudWatch also provides logs which are not AWS specific meaning that it can handle 3rd party logs from outside of AWS.

#### Roles

Roles are more secure than storing access key and secret access key on individual EC2 instances. F.e if the EC2 intance is compromised then attackers have both keys and can use them to access AWS resources. They are also easier to manage. Roles can be assigned to EC2 instance **after** it has been provisioned using CLI or AWS console. Roles are universal and can be used in **any region**

#### Instance Meta-data

Used to get information about an instance and it's address is always same http://169.254.169.254/latest/meta-data/ there is also user-data service located in  http://169.254.169.254/latest/user-data/

#### EFS

EFS supports NFSv4 protocol and can scale up to petabytes. It can suppor thousands of concurrent NFS connections. Data is stored accross availability zones.

#### Lambda

#### Placement Groups

Two types of Placement Groups

- Clustered Placement Group
- Spread Placement Group

#### EC2 pricing models:

- **On Demand**, where you pay by the second or by the hour
- **Spot**, where you set a bid price and if that spot price equals that bid price the instance will be provisioned. Also if spot price goes higher than bid price the instance will be terminated. Anything less than 1 hour and you won't be charged but if you manually terminate that instance you will be charged
- **Reserved**, where you reserve capacity. Contracts run from 12 months to 36 months. More you pay upfront the bigger discount you get.
- **Dedicated Hosts**,  where you won't be sharing any hardware with others

#### EC2 instance types:

- **F1**, Field Programmable Gate Array, *Genomics research, financial analytics, realtime video processing, big data, etc..*
- **I3**, High Speed Storage, *NoSQL, DBs, Data Warehousing, etc..*
- **G3**,  Graphics Intensive, *Video Encoding / 3D Application Streaming*
- **H1**, High Disk Throughput, *MapReduce-based workloads, distributed file systems (HDSF, MapR-FS)*
- **T2**, Lowest Cost, General Purpose, *Web Servers / Small DBs*
- **D2**, Dense Storage, *Fileservers / Data Warehousing / Hadoop*
- **R4**, Memory Optimized, *Memory Intensive Apps / DBs*
- **M5**, General Purpose, *Application Servers*
- **C5**, Compute Optimized, *CPU Intensive Apps / DBs*
- **P3**, Graphics/General Purpose GPU, *Machine Learning, Bitcoin mining, etc...*
- **X1**, Memory Optimized, *SAP HANA / Apache Spark etc...*

Remember: **FIGHTDRMCPX**

#### EBS types

- **SSD**, General Purpose - GP2 - (Up to 10 000 IOPS)
- **SSD**, Provisioned IOPS, IO1 - (More than 10 000 IOPS)
- **HDD**, Throughput Optimized - ST1 - frequently accessed workloads
- **HDD**, Cold - SC1 - less frequently accessed data (f.e file server)
- **HDD**, Magnetic - Standard - cheap, infrequently accessed storage

EC2 boot volumes can be GP2, IO1 or HDD Magnetic. EBS Volume can't be mounted to multiple EC2 instances; instead use EFS or S3.

