## Route53

Route53 is global service which contains six different routing policies

* Simple Routing Policy
* Weighted Routing Policy
* Latency Routing Policy
* Failover Routing Policy
* Geolocation Routing Policy
* Multivalue Answer Routing Policy

There are 50 domain names available by default, however it is a soft limit that can be raised by contacting AWS support.

### Simple Routing Policy

Simple Routing Policy can have only one record with multiple IP addresses. If you specify multiple values in a record, it will return values in a random order. 

### Weighted Routing Policy

Weighted Routing Policy can split traffic accross multiple IP addresses with predefined weight f.e 25% traffic to eu-west-1 and 75% traffic to eu-west-2. Weight can be a number which are added together and divided with weight. F.e if you give first record 25, second 170 and third  1000, the first ones weight is calculated with 25 / 25+170+1000 = 0.021%. 

### Latency Routing Policy

Latency based routing allows to route traffic based on the lowest  network latency for the end user. (f.e which region gives them the fastest response time)

### Failover Routing Policy

Failover Routing Policies are used to create active/passive set up. f.e if the primary site is located in eu-west-2 and secondary site is in ap-southeast-2 then the Route53 can be configured to monitor the primary site using health check. If primary site fails the health check, all the traffic will be redirected to secondary site located in ap-southeast-2.

### Geolocation Routing Policy

Geolocation Routing Policy routes traffic depending on end users locations which DNS queries originates. f.e european customers can be configured to connect eu-west-1 and US customers to us-east-1

### Multivalue Answer Routing Policy

Multivalue Answer Routing Policy allows to route traffic approximately randomly to multiple resource f.e EC2 instances. Multivalue answer record can be created for each resource and optionally assiociate with an health check with each record. F.e suppose you manage an HTTP web service with a dozen web servers that each have their own IP address. No one web server could handle all of the traffic but if you create a dozen multivalue answer records then Amazon Route53 responds to DNS queries with up to eight healty records in response to each DNS query. Route53 gives different answers to different DNS resolvers. If one server becomes unavailable after resolver caches a response, client software can try another IP address in the response

