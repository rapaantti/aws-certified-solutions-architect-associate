## Databases on AWS

### RDS - Backups, Multi-AZ & Read Replicas

Two different types of backups; Automated Backups and Database Snapshots

#### Automated Backups

These allows to recover database to any point in time within a "retention period". This period can be between one and 35 days (1-35). Automated Backups will take a full daily snapshots and will also store transaction logs throughout the day. **In case of a recovery AWS will first choose the most recent daily backup and then apply transaction logs relevant to that day. ** This allows you to do a point in time recovery down to a second within the redention period.

Automated Backups are **enabled by default**. The backup data is stored in S3 and free storage space equal the size of your database is complimentary. So if you have an RDS instance of 10Gb then 10Gb is offered free of charge for backups. Backups are taken within defined window. During the backup window, storage I/O may be suspended while your data is being backed up and you may experience elevated latency.

**When the RDS instance is deleted, backups accosiated to that instance will be also deleted.**

#### Database Snapshots

These are done manually by user. **They are stored even after you delete the RDS instance** unlike automated backups. Think of these as a standalone files.

#### Restoring Backups

Whenever RDS is restored from backups, the restored version of the database will be new RDS instance with a new DNS endpoint. Like this:

```
original.eu-west-1.rds.amazonaws.com => restored.eu-west-1.rds.amazonaws.com
```

#### Encryption

Encryption at rest is supported for MySQL, Oracle, SQL Server, PostgreSQL, MariaDB and Aurora. Encryption is done using the AWS Key Management Service (KMS). Once your RDS instance is encrypted, the data stored at the rest in the underlying storage is encrypted, as are its automated backups, read replicas, and snapshots.

Currently encrypting an existing DB instance is not supported. To use Amazon RDS encryption for an existing database, one must first create a snapshot, make a copy of that snapshot and encrypt the copy.


#### Multi-AZ

Multi-AZ allows to have an exact copy of database in another Availability Zone. AWS handles the replication so when primary database is written to, write will be automatically synchronized to standby database. In the event of planned database maintenance, database failure or even AZ failure, Amazon RDS will automatically failover to the standby database.

**Multi-AZ is for disaster recovery only and not for improving performance.** If database is located in eu-west-1 then it's replica can be located for example in eu-west-2. If eu-west-1 somehow fails, Amazon will detect this and update DNS address to point eu-west-2. **Never deal with IP addresses with RDS, use DNS enpoints only.**

Available for:

- SQL Server
- Oracle
- MySQL Server
- PostgreSQL
- MariaDB
- Aurora

#### Read Replica

Read Replicas allows to have read-only copy of a database. This is achieved by **using async replication** from the primary RDS instance to the read replica(s). **Read Replicas are used for very read-heavy database workloads** (Scale out).

Available for:

- MySQL Server
- PostgreSQL
- MariaDB
- Aurora

Bullets:

- **Used for scaling and not rod disaster recovery**. 
- Also automatic backups **must be turned on** in order to deploy a Read Replica.
- Max. five (5) Read Replicas are allowed. 
- Read Replicas can have Read Replicas but this will have some latency.
- Each Read Replica will have its **own DNS endpoint**. 
- Read Replicas that have Multi-AZ are allowed. Multi-AZ source databases allows to create Read Replicas. 
- Read Replicas can be promoted to be their own databases but this will break the replication.
- Read Replica can be located in another region.

### DynamoDB

Amazon DynamoDB is a fast and flexible NoSQL database service for all applications that need consistent, single-digit millisecond latency at any scale. It's fully managed database and supports both document and key-value data models. It's flexible data model and reliable performance make it a great fit for mobile, web, gaming, ad-tech, IoT, and many other applications.

Facts:

- Stored on SSD storage
- Spread across three geographically distinct data centres (three multiple AZs).
- Eventual Consistent Reads (By default) - *Consistency across all copies of data is usually reached within a second. Repeating a read after a short time should return the updated data (**Best read performance**)*
- Strongly Consistent Reads - *A strongly consistent read returns a result that reflects all writes that received a succesful response prior to read*

#### Pricing

**Pricing uses provisioned throughput capacity** which means that it costs $0.0065 per hour for every 10 units of write throughput and $0.0065 per hour for every 50 units of read throughput. There is also a storage cost of 0.25$ per Gb per month.

How to calculate throughtput:

F.e application performs 1 million writes and 1 million reads per day while storing 3Gb of data. First calculate how many writes and reads per second is needed. 1 million evenly spread writes per day is equivalen to:

```
Seconds in a day: 24 (hours) / 60 (minutes) / 60 (seconds) = 86400
Writes per second: 1 000 000 (writes) /  86400 s = 11.6 w/s
```
A DynamoDB Write Capacity Unit can handle 1 write per second, so in this example 12 WCUs are needed. Also with those same numbers 12 Read Capacity Units are needed. With RCUs billing is calculated blocks of 50 and WCUs blocks of 10.

```
WCU: ($0.0065 / 10) x 12 x 24 = $0.187
RCU: ($0.0065 / 50) x 12 x 24 = $0.0374
Storage: $0.25 * 3 Gb = $0.75
Total: $0.9744
```

### RedShift

Amazon RedShift is a fast and powerful, fully managed, petabyt-scale data warehouse service in the cloud. Customers can start small for just $0.25 per hour with no commitments or upfront costs and scale to a petabyte or more for $1000 per terabyte per year, less than tenth of most other data warehousing solutions.

Instead of storing data as a series of rows, Amazon Redshift organizes the data by column. Unlike row-based systems, which are ideal for transaction processing, column-based systems are ideal for data warehousing and analytics, where queries often involve aggregates performed over large data sets. Since only the columns involved in the queries are processed and columnar data is stored sequentially on the storage media, column-based systems require far fewer I/Os, greatly improving query performance.

MPP (Massively Parallel Processing) is used when Multi-Node configurated. Redshift automatically distributes data and query load across all nodes. 

#### Configuration

Starts out with a single node that can have up to 160 Gb worth of data. Scaling is done via Multi-Node configuration. This type of configuration consists of a Leader Node (which manages client connections and receives queries) and Compute Nodes (which store data and perform queries and computations). Max. number of Compute Nodes is 128.

#### Pricing

Pricing is based on Compute Node Hours (total number of hours you run across all your compute nodes for the billing period. You are billed for 1 unit per node per hour, so 3-node data warehouse cluster running persistenly for an entire month would incure 2160 instance hours. Leader Node is free of charge). Backups and data transfer (only within a VPC, not outside it) are also chargeable

#### Security

Everything that's communicated to redshift is encrypted in transit using SSL and rest is encrypted using AES-256. By default RedShift takes care of Key Management but custom management is available through HSM.

#### Availability

Currently available only in 1 AZ but snapshots can be restored to new AZ in the event of an outage.

### Elasticache

Elasticache is a web service that makes it easy to deploy, operate, and scale an in-memory cache in the cloud. The service improves the performance of web application by allowing you to retrieve information from fast, managed, in-memory caches, instead of relying entirely on slower disk-based databases. Used to significantly improve latency and throughput for many read-heavy application workloads (such as social networking, gaming, media sharing and Q&A portals) or compute intensive workloads (such as recommendation engine)

Caching improves application performance by storing critical pieces of data in memory for low-latency access. Cached informatioin may include the results of I/O-intensive database queries or the result of computationally-intensive calculations. There are two types of elasticache; memcached and redis

#### Memcached

Widely adopted memory object caching system. Elasticache is protocol compliant with memcached, so popular tools that is used will work seamlessly with the service

#### Redis

Popular open-source in-memory key-value store that supports data structures such as sorted sets and lists. Elasticache supports master / slave replication and Multi-AZ which can be used to achieve cross AZ redundancy

### Aurora

Amazon Aurora is MySQL-compatible, relational database engine that combines the speed and availability of high-end commercial databases with the simplicity and cost-effectiveness of open source databases. Amazon aurora provides up to five times beter performance than MySQL at a price point one tenth that of commercial database while delivering similar performance and availability.

#### Scaling

Starts with 10Gb database that scales in 10Gb increments and storage autoscales so no need to worry about provisioning more storage. Compute resources can scale up to 32vCPUs and 244Gb of memory. Scaling does not have downtime so no need for maintenance window. Aurora maintains two copies of your data that is contained in each AZ with minimum of 3 AZ. This means 6 copies of data. It handles loss of two copies of data without affecting database write availability and up to three copies of data without affecting read availability so this makes it really highly redundant. Aurora storage is also self-healing which means that data blocks and disks are continuosly scanned for errors and repaired automatically.

#### Replicas

There are two types of replicas currently available; Aurora Replicas, MySQL Read Replicas. Aurora Replicas are separate Aurora database from original database and database can have up to 15 of those. MySQL Read Replicas are the same as normally and database can have up to 5 of those. **Failover will automatically occur to Aurora Replicas but not for MySQL Read Replicas**.
