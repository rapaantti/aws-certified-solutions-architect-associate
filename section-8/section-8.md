## VPC (Virtual Private Cloud)

Think of a VPC as a virtual data centre in the cloud. Every region in the world has a default VPC. Amazon VPC lets you provision a logically isolated section of the AWS Cloud where you can launch AWS resources in a virtual network that you define. You have complete control over your virtual networking environment, including selection of your own IP adress range, creation of subnets, and configuration of route tables and network gateways. VPC network configurations are easily customizeable. F.e public-facing subnet can be created for webservers that have access to the Internet and private-facing subnet for backend systems such as databases or application servers with no Internet access. Multiple layers of security can be leveraged using VPCs (including security groups, network access control lists)

Hardware Virtual Private Network (VPN) connection between corporate datacenter and VPC can be created and leverage the AWS cloud as an extension of your corporate datacenter.

**VPCs always use 10.0.0.0 network address range.**

There are two ways to connect into a VPC; Internet Gateway and Virtual Private Gateway. Internet Gateway provides Internet access and Virtual Private Gateway is used when connecting via VPN. Then all the traffic is sent to a router which uses routing table and then it goes to Network ACL (Access Control List). After that it either hits to public or private subnets. 

** One subnet equals one Availability Zone**

When creating a VPC it will always generate a Security Group, a Network ACL, default route but it won't create any subnets.

The first four IP addresses and the last IP address in each subnet CIDR block are not available to use and thus cannot be assigned to an instance.  F.e in a subnet with CIDR block 10.0.0.0/24, the following five IP addresses are reserved:

- **10.0.0.0:** *Network address*
- **10.0.0.1**: *Reserved by AWS for the VPC Router*
- **10.0.0.2**: *Reserved by AWS. The IP address of the DNS server is always the base of the VPC Network range plus two.*
- **10.0.0.3**: *Reserved by AWS for future use*
- **10.0.0.255**: *Network broadcast address. AWS does not support broadcast in a VPC, therefore they reserve this adress*

### What to do with a VPC?

- Instances can be launched into a chosen subnet
- Custom IP address ranges can be configured to each subnet. 
- Route tables can be configured between subnets *(which subnets are allowed to communicate with each other)*
- Create an Internet Gateway and attach it to a VPC, **only on per VPC**
- Much better security control over AWS resources *(subnets can block specific IP addresses, Network ACL can block specific IP addresses, instances can be moved to private subnets , security groups can be attached to them)*

### Default VPC vs Custom VPC

Default VPC is user friendly, allowing to immediately deploy instances and all subnets have a route to the Internet. Each EC2 instances will have both public and private IP adress when deployed to default VPC. With custom VPC instances will have private IP address only

### VPC Peering

Allows to connect one VPC with another VPC via a direct network route using private IP adresses. They will behave like they are in a same private network when you peer VPCs. Multiple AWS accounts can be peer using VPCs and they behave like they are in the same account. **VPC doesn't have transitive peering** meaning that if VPC A is peered with VPC B and VPC C then VPC B and VPC C **can not **communicate without peering them together


### NAT Instances

When creating a NAT Instance source / destination check on the instance **must be disabled**. To provide Internet access to private subnets NAT Instances must be in a public subnet then there must be a route out of the private subnet to the NAT instance in order for this to work. The amount of traffic that NAT instance can support depends on the size of the instance. So if there are any bottlenecks with NAT instance, increase the size of the instance. High availability can be created using Autoscaling Groups, multiple subnets in different AZ and a script to automate failover. NAT instances are always behind a Security Group.

### NAT Gateways

**NAT Gateways are preferred** by businesses and enteprice customers because they scale automatically up to 10 Gb/s and there is no need to install patches, worry about security groups and no need to install antivirus software. They are managed  by Amazon and are automatically assigned a public IP address. Remember to update route tables to point NAT Gateways. Use NAT Gateways in multiple AZ to have some redundancy in terms of availability. There is no need to disable source / destination checks. They are more secure than NAT Instances because they don't have ssh access.

### Network Access Control Lists

Just like Security Groups, Network Access Control Lists (NACL) **can only be deployed into one VPC**. By default everything is denied on both inbound and outbound rules when you create a NACL. When adding inbound / outbound rules, Amazon recomends to start the rules with 100 IPv4 and then 101 for IPv6 and then go up in increments of 100. NACL is stateless so both inbound and outbound rules have to be allowed seperately (unlike Security Groups which are stateless). When creating VPCs it will automatically create a default NACL and by default it allows all outbound and inbound traffic. Each subnet in VPC must be associated with a NACL and by default its default NACL. NACL contains numbered list of rules that is evaluated in order, starting with the lowest number. Specific IP addresses can be blocked with NACL not Security Groups.

### Security Groups

Nothing here.

### VPC Flow Logs

VPC Flow Logs is a feature that enables to capture information about the IP traffic going to and from your network interfaces in your VPC. Flow log data is stored using Amazon CloudWatch Logs. Flow Logs can be created at  three levels:

- VPC
- Subnet
- Network Interface Level
 
 Flow Logs can not be enabled for VPC that are peered with VPC unless the peer VPC is in same account. Flow Logs cannot  be tagged. Configuration is not editable after creation. f.e IAM role cannot be changed.
 
 Not all IP traffic is monitored:
 
 - Traffic generated by instances when they contact the Amazon DNS server. But if own DNS server is used, then all traffic is logged.
 - Traffic genearated  by Windows instance for Amazon Windows  license activation
 - Traffic to and from 169.254.169.254 for instance metadata
 - DHCP traffic
 - Traffic to reserved IP address for the default VPC router
 
 
### NATs vs Bastions

A NAT is used to provide internet traffic to EC2 instances in private subnets and bastion is used to securely administer EC2 instances using SSH or RDP in private subnets
