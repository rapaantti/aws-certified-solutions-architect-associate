## SQS - Simple Queue Service

Amazon SQS is a web service that gives you access to  a message queue that can be used to store messages while waiting for a computer to process them. Amazon SQS is a distributed queue system that enables web  service applications to quickly and reliably queue messages that one component in the application generates to be consumed by another component. A queue is a temporary repository for messages that are awaiting processing.

SQS can decouble the components of an application so they run independently, easing message management between components. Any component of a distributed application can store messages in the queue. Message can contain up to 256Kb of text in any format. Any component can later retrieve the messages programmatically using the SQS API.

The queue acts as a buffer between components producing and saving data, and the component receiving the data for processing. This means the queue resolves issues that arise if the producer is producing work faster than the consumer can process it, or if the producer or consumer are only intermittently connected to the network.

### Queue Types

There are two types of Queue:

- Standart Queues (default)
- FIFO Queues (First-In-First-Out)

#### Standard Queues

SQS offers standard as the default queue type. A standard queue can have nearly-unlimited number of transactions per second. Standard Queues quarantee that a message is delivered at least once. However, occasionally (because of highly-distributed architecture that allows high throughput), more than one copy of a message might be delivered out of order. Standard queues provide best-effort ordering which ensures that messages are generally delivered in the same order as they are sent.

#### FIFO Queues

The FIFO queue complements the standard queue. The most important features of this queue type are FIFO delivery and exactly one processing: The order in which messages are sent and received is strictly preserved and a message is delivered once and remains available until a consumer processes and deletes it; dublicates are not introduced into the queue. FIFO queues also support message groups that allow multiple ordered message groups within a single queue. FIFO queues are limited to 300 transactions per second (TPS). but have all the capabilities of standard  queues.


#### SQS Facts

- SQS is pull-based, not pushed-based system.
- Messages are max. 256Kb in size
- Messages can be kept in the queue from 1 minute to 14 days
- Default retention period is 4 days
- SQS guarantees that your messages will be processed at least once

SQS Visibility timeout is the amount of time that the message is invisible in the SQS queue after a reader picks up that message. Provided the job is processed before the visibility timeout expires, the message will then be deleted from the queue. If the job is not processed within that time, the message will become visible again and another reader will process it. This could result in the same  message being delivered twice. Default visibility timout is 30 seconds and should be increased if task takes more than that. Maximum is 12 hours.

SQS Long polling is a way to retrieve messages from your SQS queues. Reqular short polling returns immediately (even if the message queue being polled is empty), long polling doesn't return a response until message arrives in the message queue, or the long poll times out so long polling can save money.

## SWF - Simple Workflow Service

Amazon Simple Workflow Service is a web service that makes it easy to coordinate work across distributed application components. SWF enables applications for a range of use cases, including media processing, web application back-ends, business process workflows, and analytics pipelines, to be designed as coordination of tasks.

Tasks represent invocations of various processing steps in an application which can be performed by executablee code, web service calls, human actions, and scripts.

### Workers

Workers are programs that interact with SWF to get tasks, process received tasks, and return the results.

### Decider

Decider is a program that control the coordination of tasks, i.e their ordering, concurrency and scheduling according to the application logic.

The workers and the deciders can run on cloud infrastructure, such as EC2, or on machines behind firewalls. SWF brokers the interactions between workers and the decider. It allows the decider to get consistent views into the progress of tasks and to initiate new tasks in an outgoing manner.

At the same time, SWF stores tasks, assigns them to workers when they are ready, and monitors their progress. It ensures that a task is assigned only once and is never duplicated. Since SWF maintains the application's state durably, workers and deciders don't have to keep track of  execution state. They can run independently, and scale quickly.

### SWF Domains

Workflow and activity types and the workflow execution itself are all scoped to a domain. Domains  isolate a set of types, executions, and task lists from other within the same account.

Domain can be registered using the AWS Management Console or by using the RegisterDomain action in the SWF API.

Maximum workflow can be  1 year and the value is always measured in seconds.

## SQS vs SWF

- SWF presents a task-oriented API, whereas SQS offers a message-oriented API. 
- SWF ensures that a task is assigned only once and is never duplicated. SQS need duplicate message handling and it can process messages twice.
- SWF keeps track of all the tasks and events in an application. With SQS application level tracking needs to be implemented especially if application uses multiple queues.

## SNS - Simple Notification Service

Amazon Simple Notification Service is a web service that makes it easy to set up, operate, and sent notifications from the cloud. It provides developers with highly scalable, flexible, and cost-effective capability to publish messages from an application and immediately deliver them to subscribers or other applications.

Besides pushing cloud notifications directly to mobile devices, SNS can also deliver notifications by SMS text message or email, to SQS queues, or to any HTTP endpoint. SNS notifications can also trigger Lambda functions. When a message is published to an SNS topic that has a Lambda function subscribed to it, the lambda function isinvoked with  the payload of the published message. The lambda function receives the message payload as an input parameter and can manipulate the information in the message, publish the message to other SNS topics, or send the message to other AWS service.

SNS allows to group multiple recipients using topics. A topic is an "access point" for allowing recipients to dynamically subscribe for identical copies of the same notification. One topic can support deliveries to multiple endpoint types. F.e iOS, android and SMS recipients can be groupped together and when publishing once to a topic, SNS delivers appropriately formatted copies of message to each subscriber.

### SNS Benefits

- Instantaneous, push-based delivery (no polling)
- Simple APIs and easy integration with applications
- Flexible message delivery over multiple transport protocols
- Inexpensive, pay-as-you-go model with no up-front costs
- Web-based AWS Management Console offers the simplicity of a point-and-click interface

### Pricing

Users pay $0.50 per 1 million SNS Requests and after that it's:

- $0.06 per 100k SNS over HTTP
- $0.75 per 100 SNS over SMS
- $2.00 per 100k SNS over EMAIL

## SNS vs SQS

- Both messaging services in AWS
- SNS Push
- SQS Polls

## Elastic Transcoder

Elastic Transcoder is a Media Transcoder in the cloud. It allows to convert media files from their original source format in to different formats that will play on smartphones, tablets, PC's etc... It provides transcoding presets for popular output formats, which means that you don't need to guess about which settings work best on particular devices. Pricing is based on the minutes that transcoder is using and the resolution at which is transcoded.

## API Gateway

Amazon API Gateway is fully managed service that makes it easy for developers to publish, maintain, monitor, and secure APIs at any scale. With a few clicks in the AWS Magement Console, API that acts as "front door" for applications can be created to access data, business logic, or functionality from back-end services such as applications running on EC2, lambda or any web application.

API Gateway supports enpoint's response caching. With that the number of calls made to endpoint can be reduced. This affects positively to the latency. When caching is enabled, API Gateway caches responses from the enpoint fo a specific TLL (time-to-live) period in seconds. API Gateway then responds to the request by looking up the endpoint response from the cache instead of making request to the endpoint.

### Benefits

- Low cost & efficient
- Scales effortlessly
- Can throttle requests to prevent attacks
- Can connect to CloudWatch to log all requests

## Kinesis

Amazon Kinesis is a platform on AWS to send streaming data. Kinesis makes it easy to load and analyze streaming data, and also providing the ability for to build own custom applications for business needs. There are three core services in Kinesis:

- Kinesis Streams
- Kinesis Firehose
- Kinesis Analytics

### Kinesis Streams

Kinesis Streams consists of shards which can do 5 transactions per second for reads up to maximum total data read rate of 2Mb per second and up to 1000 records per second for writes, up to a maximum total data write rate of 1Mb per second (including partition keys). The data capacity of streams is  a function of the number of shards that is specified to the stream.  The total capacity of the stream is the sum of capacities of it's shard.

### Kinesis Firehose

Kinesis Firehose is completely automated so there is no need to worry about data consumers, shards, etc... So when the data comes to Firehose it's either analyzed using lambda or it is sent directly to S3.

### Kinesis Analytics

Kinesis Analytics is a way of analyzing the data inside Kinesis

 